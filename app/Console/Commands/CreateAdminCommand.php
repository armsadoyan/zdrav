<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin {mail} {password} {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates admin for the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();
        $user->fill([
            'name' => $this->argument('username'),
            'email' => $this->argument('mail'),
            'password' => Hash::make($this->argument('password'))
        ]);
        $user->save();

        $user->role()->associate(Role::where('slug', 'admin')->first());

        return true;
    }
}
