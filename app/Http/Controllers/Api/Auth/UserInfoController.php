<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserInfoController extends Controller
{
    public function info()
    {
        $user = \Auth::user();
        return response()->json(['success' => $user], 200);
    }
}
