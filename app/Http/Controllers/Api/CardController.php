<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\CardFieldValue;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class CardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __construct()
    {
        Auth::loginUsingId(1);
    }

    public function form(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $response = CardFieldValue::addOrUpdateCard($data);
            return response()->json($response, $response['status']);
        }

        $response = CardFieldValue::getCard();
        return response()->json($response, $response['status']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
//    public function getCard(){
//        $response = CardFieldValue::getCard();
//        return response()->json($response, $response['status']);
//    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $response = CardFieldValue::destroy($id);
        return response()->json($response, $response['status']);
    }
}
