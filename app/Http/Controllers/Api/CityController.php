<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function index()
    {
        return response()->json(City::all());
    }

    public function show($id)
    {
        return response()->json(City::findOrFail($id));
    }
}
