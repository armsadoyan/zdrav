<?php

namespace App\Http\Controllers\Api;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function index()
    {
        return Country::all();
    }

    public function show($country_id)
    {
        $country = Country::with('states')->findOrFail($country_id);
        return $country;
    }
}
