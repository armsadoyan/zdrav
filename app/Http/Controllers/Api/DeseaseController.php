<?php

namespace App\Http\Controllers\Api;

use App\Facades\DeseaseFacade as Desease;
use App\Services\DeseaseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeseaseController extends Controller
{
    protected $service;

    public function __construct(DeseaseService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $deseases = $this->service->all();
        return response()->json($deseases);
    }


}
