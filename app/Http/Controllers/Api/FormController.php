<?php

namespace App\Http\Controllers\Api;

use App\Models\Form;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{
    public function index()
    {
        if (request()->has('name')) {
            $forms = Form::where('form_name', request('name'))->where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();
        } else {
            $forms = Form::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();
        }

        return response()->json($forms);
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $form = new Form();
        $form->content = json_encode($request->all());
        $form->user_id = $user->id;
        $form->form_name = $request->keys()[0];
        $form->save();

        return response()->json($form);
    }
}
