<?php

namespace App\Http\Controllers\Api;

use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __construct()
    {
//        $this->middleware('auth:api');
        Auth::loginUsingId(1);
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $data = $request->all();
            $response = Gallery::addImages($data);
            return response()->json($response, $response['status']);
        }

        $response = Gallery::getMyGallery();
        return response()->json($response, $response['status']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhotosInModeration(){
        $response = Gallery::getPhotosInModeration();
        return response()->json($response, $response['status']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePhotoStatus(Request $request){
        $data = $request->all();
        $response = Gallery::changePhotoStatus($data);
        return response()->json($response, $response['status']);
    }

//    /**
//     *
//     */
    public function getMyGallery(){
        $response = Gallery::getMyGallery();
        return response()->json($response, $response['status']);
    }
}
