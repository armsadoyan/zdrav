<?php

namespace App\Http\Controllers\Api;

use App\Models\Info;
use App\Models\InfoModeration;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class InfoController extends Controller
{
    /**
     * InfoController constructor.
     */
    public function __construct()
    {
        Auth::loginUsingId(1);
    }

    /**]
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addInfo(Request $request){
        $data = $request->post();
        $response = Info::createOrUpdate($request, $data);
        return response()->json($response, $response['status']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInfo($id = ""){
        if($id == "infoform"){
            $id = "";
        }
        $response = Info::getInfo($id);
        return response()->json($response, $response['status']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = Info::destroy($id);
        return response()->json($response, $response['status']);
    }

    public function acceptOrDeleteModeration(Request $request){
        $data = $request->all();
        $response = InfoModeration::acceptOrDelete($data);
        return response()->json($response, $response['status']);
    }
}
