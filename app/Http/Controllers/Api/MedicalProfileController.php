<?php

namespace App\Http\Controllers\Api;

use App\Services\MedicalProfileService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedicalProfileController extends Controller
{
    protected $service;

    public function __construct(MedicalProfileService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $data = $this->service->all();

        return response()->json($data);
    }
}
