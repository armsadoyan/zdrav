<?php

namespace App\Http\Controllers\Api;

use App\Models\News;
use App\Services\NewsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    protected $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = $this->service->all();
        return response()->json($news);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = $this->service->getById($id);
        return response()->json($news);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNews(Request $request)
    {
        $data = $request->all();
        $response = News::add($data);
        return response()->json($response, $response['status']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllNews()
    {
        $response = News::getAllNews();
        return response()->json($response, $response['status']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNewsById($id)
    {
        $response = News::getNewsById($id);
        return response()->json($response, $response['status']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request,$id)
    {
        $data = $request->all();
        $response = News::edit($data,$id);
        return response()->json($response, $response['status']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = News::destroy($id);
        return response()->json($response, $response['status']);
    }
}
