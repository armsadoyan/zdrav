<?php

namespace App\Http\Controllers\Api;

use App\Contracts\ObjectInterface;
use App\Services\ObjectService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Facades\ObjectFacade as Object;

class ObjectController extends Controller
{
    protected $objectService;

    public function __construct()
    {
        $this->objectService = new ObjectService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = $this->objectService->all();

        return response()->json($objects);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->objectService->getById($id);

        return response()->json($data);
    }

    public function addImages(Request $request)
    {
        return response()->json(['success' => false]);
        
        $errors = [];
        $user = auth()->guard('api')->user();
        $this->validate($request, [
            'profileform' => 'required|array',
            'profileform.avatar' => 'required',
            'profileform.name.value' => 'required|string',
            'profileform.surename.value' => 'required|string',
            'profileform.fathername.value' => 'required|string',
            'profileform.oldpassword' => 'required|array',
            'profileform.password' => 'required|array'
        ]);

        $user->name = $request->input('profileform.name.value');
        $user->last_name = $request->input('profileform.surename.value');
        $user->father_name = $request->input('profileform.fathername.value');

        if ($request->input('profileform.avatar.value.0') != null) {
            $data = $request->input('profileform.avatar.value.0');

            $img = substr($data['url'], strpos($data['url'], ",")+1);
            $filename = $user->id.'.png';
            Storage::disk('avatars')->put($filename, base64_decode($img));
            $user->avatar_url = $filename;
        }
        if ($request->input('profileform.password.value') != null and $request->input('profileform.oldpassword.value') != null) {
            if (Hash::check($request->input('profileform.oldpassword.value'), $user->password)) {
                $user->password = Hash::make($request->input('profileform.password.value'));
            } else {
                $errors['wrong_password'] = 'Введен неверный текущий пароль';
            }
        }
        $user->save();
        return response()->json(['success' => true, 'user' => $user, 'errors' => $errors]);
    }
}
