<?php

namespace App\Http\Controllers\Api;

use App\Services\TherapyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TherapyController extends Controller
{
    protected $service;

    public function __construct(TherapyService $service)
    {
        $this->service = $service;
    }

    public function index ()
    {
        $data = $this->service->all();

        return response()->json($data);
    }
}
