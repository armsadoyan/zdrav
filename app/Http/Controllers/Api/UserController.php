<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function update(Request $request)
    {
        $errors = [];
        $user = auth()->guard('api')->user();
        $this->validate($request, [
            'profileform' => 'required|array',
            'profileform.avatar' => 'required',
            'profileform.name.value' => 'required|string',
            'profileform.surename.value' => 'required|string',
            'profileform.fathername.value' => 'required|string',
            'profileform.oldpassword' => 'required|array',
            'profileform.password' => 'required|array',
        ], [
            "profileform.name.value.required" => "Поле Имя обязательное для заполнения.",
            'profileform.surename.value.required' => "Поле Фамилия обязательное для заполнения.",
            'profileform.fathername.value.required' => "Поле Отчество обязательное для заполнения.",
            'profileform.oldpassword.required' => "Поле текущий пароль обязательное для заполнения.",
            'profileform.password.required' => "Поле новый пароль обязательное для заполнения.",
        ]);

        $user->name = $request->input('profileform.name.value');
        $user->last_name = $request->input('profileform.surename.value');
        $user->father_name = $request->input('profileform.fathername.value');
        if ($request->input('profileform.avatar.value.0') != null) {
            if ($request->input('profileform.avatar.value.0.size')) {
                $imgInfo = $request->input('profileform.avatar.value.0.url');
                $imgInfo = str_replace('data:image/jpeg;base64,','',$imgInfo);
                $imgInfo = str_replace('data:image/jpg;base64,','',$imgInfo);
                $imgInfo = str_replace('data:image/png;base64,','',$imgInfo);
                $imgInfo = str_replace('data:image/bmp;base64,','',$imgInfo);
                $img = explode(',', $imgInfo);
                $ini = substr($img[0], 11);
                $type = explode(';', $ini);
                $filename = $user->id.'.png';

                if(Storage::disk('avatars')->put($filename, base64_decode($imgInfo))){
                    $user->avatar_url = $filename;
                }else{
                    return response()->json(["message" => "Write error.",
                        "errors" => ["profileform.avatar.value" => ["Disk write error "]]], 422);
                }
            }
        }
        if(!empty($request->input('profileform.password.value')) || !empty($request->input('profileform.oldpassword.value'))){
            if(strlen($request->input('profileform.password.value')) < 8){
                return response()->json(["message" => "The given data was invalid.",
                    "errors" => ["profileform.password.value" => ["Текущий пароль должен быть не менее 8 символов"]]], 422);
            }
            if(strlen($request->input('profileform.oldpassword.value')) < 8){
                return response()->json(["message" => "The given data was invalid.",
                    "errors" => ["profileform.oldpassword.value" => ["Новый пароль должен быть не менее 8 символов"]]], 422);
            }
            if($request->input('profileform.password.value') == ""){
                return response()->json(["message" => "The given data was invalid.",
                    "errors" => ["profileform.password.value" => ["Не заполнен новый пароль"]]], 422);
            }elseif($request->input('profileform.oldpassword.value') == ""){
                return response()->json(["message" => "The given data was invalid.",
                    "errors" => ["profileform.oldpassword.value" => ["Не заполнен текущий пароль"]]], 412);
            } else {
                if (Hash::check($request->input('profileform.oldpassword.value'), $user->password)) {
                    $user->password = Hash::make($request->input('profileform.password.value'));
                } else {
                    return response()->json(["message" => "The given data was invalid.",
                        "errors" => ["profileform.oldpassword.value" => ["Введен неверный текущий пароль"]]], 412);
                }
            }
        }

        $user->save();
        return response()->json(['success' => true, 'user' => $user]);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }
}
