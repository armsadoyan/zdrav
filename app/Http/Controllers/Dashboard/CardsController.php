<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardsCreationRequest;
use App\Models\Cards;
use App\Services\CardsService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class CardsController extends Controller
{

    public function __construct(CardsService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $forms = Cards::getAllCards();
        return view('admin.forms.index')->with('forms', $forms["data"]);
    }

    public function edit(Cards $news)
    {
        $this->authorize('news-update', \Auth::user());
        return view('admin.forms.edit')
            ->with('news', $news);
    }

    public function update(CardCreationRequest $request, $id)
    {
        $this->authorize('news-update', \Auth::user());
        $news = $this->service->update($request->only(['name', 'image_url']), $id);

        return redirect()->route('forms.show', ['id' => $id])->with('status', 'Анкета успешно обновлена!');
    }

    public function destroy($id)
    {
        $this->authorize('news-delete', \Auth::user());
        $this->service->delete($id);

        return redirect()->route('forms.index');
    }

    public function create(Request $request)
    {
        return view('admin.forms.create');
    }

    public function show(Cards $cards)
    {
//        $this->authorize('cards-read', \Auth::user());
        return view('admin.forms.show')->with('cards', $cards);
    }

    public function store(Request $request)
    {
        $cards = $this->service->store($request->only(['name', 'image']));
        return redirect()->route('forms.index')->with('status', 'Анкета успешно создана!');
    }
}
