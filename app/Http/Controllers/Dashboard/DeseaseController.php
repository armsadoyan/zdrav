<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Desease;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeseaseController extends Controller
{
    public function index()
    {
        $this->authorize('desease-read', \Auth::user());
        $deseases = Desease::with('medicalProfiles', 'therapies')->paginate(20);

        return view('admin.desease.index')
            ->with('deseases', $deseases);
    }

    public function show(Desease $desease)
    {
        $this->authorize('desease-read', \Auth::user());
        return view('admin.desease.show');
    }

    public function edit(Desease $desease)
    {
        $this->authorize('desease-update', \Auth::user());
        return view('admin.desease.edit');
    }

    public function update(Request $request, Desease $desease)
    {
        $this->authorize('desease-update', \Auth::user());
    }

    public function create()
    {
        $this->authorize('desease-create', \Auth::user());
        return view('admin.desease.create');
    }

    public function store(Request $request)
    {
        $this->authorize('desease-create', \Auth::user());
    }

    public function delete(Desease $desease)
    {
        $this->authorize('desease-delete', \Auth::user());
        $desease->delete();
        return redirect()->back()->with('status', 'Заболевание удалено');
    }
}
