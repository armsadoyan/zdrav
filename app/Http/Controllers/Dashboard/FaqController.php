<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\FaqRequest;
use App\Models\Faq;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index()
    {
        return view('admin.faqs.index')->with('faqs', Faq::paginate(10));
    }

    public function create()
    {
        return view('admin.faqs.create')->with('tags', Tag::all());
    }

    public function store(FaqRequest $request)
    {
        $faq = new Faq();
        $faq->fill($request->only(['title', 'description']));
        $faq->save();
        $faq->tags()->sync($request->tags);
        return redirect()->route('faqs.edit', ['id' => $faq->id]);
    }

    public function edit(Faq $faq)
    {
        return view('admin.faqs.edit')->with('tags', Tag::all())->with('faq', $faq);
    }

    public function update(FaqRequest $request, Faq $faq)
    {
        $faq->fill($request->only(['title', 'description']));
        $faq->save();
        $faq->tags()->sync($request->tags);
        return redirect()->back();
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();
        return redirect()->route('faqs.index');
    }
}
