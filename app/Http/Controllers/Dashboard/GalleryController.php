<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\GalleryCreateionRequestt;
use App\Models\Gallery;
use App\Models\PagLimit;
use App\Http\Requests\GalleryCreationRequest;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = 10;
        $query = PagLimit::where('user_id', Auth::id())->first();
        if(!empty($query)) {
            $limit = $query->pag_limit;
        }
        $galleries = Gallery::orderBy('id', 'desc')->paginate($limit);
        return view('admin.galleries.index', ['limit' => $limit])->with('galleries', $galleries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryCreationRequest $request)
    {
        $result = Gallery::create($request->inputs());
        if($result) {
            return redirect()->route('galleries.index')->with(['image' => $result, 'success' => 'true'], 200);
        }
        return back()->with(['error' => 'Something went wrong!!!'], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::where('id', $id)->first();
        if($gallery->update(['status' => 'accept'])) {
            return back()->with(['status' => 'success']);
        }
        return back()->with(['status' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gallery::where('id', $id)->delete()) {
            return back()->with(['status' => 'Фото успешно удалена']);
        }
        return back()->with(['status' => 'failed']);
    }
}
