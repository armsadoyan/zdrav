<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\InfoCreationRequest;
use App\Models\Info;
use App\Models\PagLimit;
use Auth;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function index(Request $request)
    {
        $limit = 10;
        $query = PagLimit::where('user_id', Auth::id())->first();
        if(!empty($query)) {
            $limit = $query->pag_limit;
        }
        $infos = Info::paginate($limit);
        return view('admin.infos.index', ['limit' => $limit])->with('infos', $infos);
    }

    public function create()
    {
        return view('admin.infos.create');
    }

    public function store(InfoCreationRequest $request)
    {
        $attributes = $request->only([
            'organisation_description',
            'stars',
            'payments_terms',
            'docs',
            'contraindications',
            'medical_profiles',
            'treatment_methods',
            'services',
        ]);

        $info = Info::create([
            'organisation_description' => $attributes['organisation_description'],
            'stars' => $attributes['stars'],
            'payments_terms' => $attributes['payments_terms'],
            'docs' => $attributes['docs'],
            'contraindications' => $attributes['contraindications'],
            'medical_profiles' => isset($attributes['medical_profiles']),
            'treatment_methods' => isset($attributes['treatment_methods']),
            'services' => $attributes['services'],
        ]);

        return redirect()->route('informations.show', ['id' => $info->id])->with('status', 'Успешно создана информация!');
    }

    /**
     * Display the specified resource.
     *
     * @param Info $info
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function show($id)
    {
        $info = Info::find(['id' => $id])->first();
        $this->authorize('object-read', \Auth::user());
        return view('admin.infos.show')->with('info', $info);
    }

    public function edit($id)
    {
        if ($info = Info::where('id', $id)->first()) {
            return view('admin.infos.edit')->with('info', $info);
        }
        return abort(404);
    }

    public function update($id, InfoCreationRequest $request)
    {
        $attributes = $request->only([
            'organisation_description',
            'stars',
            'payments_terms',
            'docs',
            'contraindications',
            'medical_profiles',
            'treatment_methods',
            'services',
        ]);

        $info = Info::where('id', $id)->update([
            'organisation_description' => $attributes['organisation_description'],
            'stars' => $attributes['stars'],
            'payments_terms' => $attributes['payments_terms'],
            'docs' => $attributes['docs'],
            'contraindications' => $attributes['contraindications'],
            'medical_profiles' => isset($attributes['medical_profiles']),
            'treatment_methods' => isset($attributes['treatment_methods']),
            'services' => $attributes['services'],
        ]);

        if ($info) {
            return redirect()->route('informations.show', ['id' => $id])->with('status', 'Успешно редактирована информация!');
        }
        return redirect()->route('informations.show', ['id' => $id])->with('status', 'failed');
    }

    public function destroy($id)
    {
        if (Info::where('id', $id)->delete()) {
            return back()->with(['status' => 'Информация успешно удалена']);
        }
        return back()->with(['status' => 'failed']);
    }
}
