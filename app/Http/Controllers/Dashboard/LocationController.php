<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function index()
    {
        return view('admin.locations.index')
            ->with('countries', Country::paginate(10))
            ->with('states', State::paginate(10, ['*'], 'states'))
            ->with('cities', City::paginate(10, ['*'], 'cities'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['type' => 'required|in:country,state,city']);
        $model = $this->getLocationModelInstance($request->type);
        $this->validateLocationByType($request, $request->type);
        $model->fill($request->all());
        $model->save();
        return redirect()->back();
    }

    private function getLocationModelInstance($type)
    {
        switch($type) {
            case 'country':
                return new Country();
                break;
            case 'state':
                return new State();
                break;
            case 'city':
                return new City();
                break;
        }
    }

    private function validateLocationByType(Request $request, $type)
    {
        switch($type){
            case 'country':
                $this->validate($request, ['name' => 'required|string']);
                break;
            case 'state':
                $this->validate($request, ['name' => 'required|string', 'country_id' => 'required|exists:countries,id']);
                break;
            case 'city':
                $this->validate($request, ['name' => 'required|string', 'state_id' => 'required|exists:states,id']);
                break;
        }
    }

}
