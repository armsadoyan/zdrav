<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsCreationRequest;
use App\Models\News;
use App\Services\NewsService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    protected $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('news-read', \Auth::user());
        $news = $this->service->getWithPagination();

        return view('admin.news.index')
            ->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('news-read', \Auth::user());
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsCreationRequest $request
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function store(NewsCreationRequest $request)
    {
        $this->authorize('news-create', \Auth::user());
        $news = $this->service->store($request->only(['image', 'title', 'meta_description', 'meta_keywords', 'short_text', 'full_text', 'date', 'is_visible', 'is_published', 'picture']));

        return redirect()->route('news.show', ['id' => $news->id])->with('status', 'Успешно создана новость!');
    }

    /**
     * Display the specified resource.
     *
     * @param News $news
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function show(News $news)
    {
        $this->authorize('news-read', \Auth::user());
        return view('admin.news.show')->with('news', $news);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param News $news
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function edit(News $news)
    {
        $this->authorize('news-update', \Auth::user());
        return view('admin.news.edit')
            ->with('news', $news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsCreationRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function update(NewsCreationRequest $request, $id)
    {
        $this->authorize('news-update', \Auth::user());
        $news = $this->service->update($request->only(['title', 'meta_description', 'meta_keywords', 'short_text', 'full_text', 'date', 'is_visible', 'is_published']), $id);

        return redirect()->route('news.show', ['id' => $id])->with('status', 'Новость успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('news-delete', \Auth::user());
        $this->service->delete($id);

        return redirect()->route('news.index');
    }
}
