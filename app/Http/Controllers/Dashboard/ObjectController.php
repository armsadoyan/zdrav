<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\ObjectCreateRequest;
use App\Models\City;
use App\Models\Country;
use App\Http\Controllers\Controller;
use App\Models\Place;
use App\Models\State;
use App\Models\PagLimit;
use App\Models\MedicalProfile;
use App\Models\Therapy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class ObjectController extends Controller
{
    public function index(Request $request)
    {
        $limit = 10;
        $query = PagLimit::where('user_id', Auth::id())->first();
        if(!empty($query)) {
            $limit = $query->pag_limit;
        }
        $objects = Place::withoutGlobalScope('published')->with('rooms')->paginate($limit);
        return view('admin.objects.index', ['limit' => $limit])->with('objects', $objects);
    }

    public function changePaginationLimit(Request $request) {
        $pageLimit = $request->pag_limit ? $request->pag_limit : 10;
        $limit = PagLimit::where('user_id', Auth::id())->first();
        if(empty($limit)) {
            PagLimit::create(['user_id' => Auth::id(), 'pag_limit' => $pageLimit]);
        } else {
            PagLimit::where('user_id', Auth::id())->update(['pag_limit' => $pageLimit]);
        }
        $objects = Place::withoutGlobalScope('published')->with('rooms')->paginate($pageLimit);
        return back()->withInput(['limit' => $pageLimit, 'objects', $objects]);
    }

    public function search(Request $request) {
        if($request->ajax())
        {
            $limit = 10;
            $pag_query = PagLimit::where('user_id', Auth::id())->first();
            if(!empty($pag_query)) {
                $limit = $pag_query->pag_limit;
            }
            $output = '';
            $query = $request->get('query');
            if($query != '') {
                $data = Place::withoutGlobalScope('published')->with('rooms')
                    ->where('name', 'like', '%'.$query.'%')
                    ->paginate($limit)
                    ->setPath('');
                $data->appends($request->all());
            } else {
                $data = Place::withoutGlobalScope('published')->with('rooms')->paginate($limit);
                $data->appends($request->all());
            }
            $total_row = $data->count();
            if($total_row > 0)
            {
                foreach($data as $row)
                {
                    $output .= ' <tr><td>'.$row->name.'</td><td>'.$row->id.'</td></tr>';
                }
            }  else {
                $output = '<tr><td align="center" colspan="5">No Data Found</td> </tr>';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );

            echo json_encode($data);
        }
    }
    public function create()
    {
        $countries = Country::all();
        $states = State::all();
        $cities = City::all();
        $medicalProfiles = MedicalProfile::all();
        $therapies = Therapy::all();
        return view('admin.objects.create')
            ->with('countries', $countries)
            ->with('states', $states)
            ->with('cities', $cities)
            ->with('medicalProfiles', $medicalProfiles)
            ->with('therapies', $therapies);
    }

    public function store(ObjectCreateRequest $request)
    {
        $object = new Place();
        $object->fill($request->only(['name', 'url', 'country_id', 'state_id', 'city_id', 'description', 'stars', 'payment_conditions', 'required_documents', 'visa_information', 'contraindications']));
        $object->is_published = $request->is_published ? 1 : 0;
        $object->save();

        $object->medicalProfiles()->sync($request->medical_profiles);
        $object->therapies()->sync($request->therapies);

        return redirect()->route('objects.edit', ['id' => $object->id]);
    }

    public function edit(Place $object)
    {
        $countries = Country::all();
        $states = State::all();
        $cities = City::all();
        $medicalProfiles = MedicalProfile::all();
        $therapies = Therapy::all();
        return view('admin.objects.edit')
            ->with('countries', $countries)
            ->with('states', $states)
            ->with('cities', $cities)
            ->with('medicalProfiles', $medicalProfiles)
            ->with('therapies', $therapies)
            ->with('object', $object);
    }

    public function update(ObjectCreateRequest $request, Place $object)
    {
        $object->fill($request->only(['name', 'url', 'country_id', 'state_id', 'city_id', 'description', 'stars', 'payment_conditions', 'required_documents', 'visa_information', 'contraindications']));
        $object->is_published = $request->is_published ? 1 : 0;
        $object->save();
        $object->medicalProfiles()->sync($request->medical_profiles);
        $object->therapies()->sync($request->therapies);

        return redirect()->back();
    }

    public function destroy(Place $object)
    {
        $object->delete();
        return redirect()->route('objects.show');
    }
}
