<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\RoleCreationRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class RoleController extends Controller
{
    protected $service;

    public function __construct(RoleService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('role-read', \Auth::user());

        $roles = Role::all();

        return view('admin.role.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('role-create', \Auth::user());

        $permissions = $this->getPermissions();

        return view('admin.role.create')
            ->with('permissions', $permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleCreationRequest $request)
    {
        $this->authorize('role-create', \Auth::user());
        $role = Role::create($request->only(['name', 'slug']));
        $role->permissions()->attach(array_keys($request->input('permission')));

        return redirect()->route('roles.show', ['id' => $role->id])->with('status', 'Создана запись #' . $role->id);
    }

    /**
     * Display the specified resource.
     *
     * @param News $news
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $this->authorize('role-read', \Auth::user());
        $permissions = $this->getPermissions();
        return view('admin.role.show')->with('role', $role)->with('permissions', $permissions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param News $news
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorize('role-update', \Auth::user());

        if (!$role->is_editable) {
            return redirect()->back()->with('warning', 'Не может быть изменено');
        }

        $permissions = $this->getPermissions();

        return view('admin.role.edit')->with('role', $role)->with('permissions', $permissions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleCreationRequest $request, Role $role)
    {
        $this->authorize('news-update', \Auth::user());

        if (!$role->is_editable) {
            return redirect()->back()->with('warning', 'Не может быть изменено');
        }

        $role->update($request->only([
            'name',
            'slug'
        ]));

        $role->permissions()->sync(array_keys($request->input('permission')));

        return redirect()->route('roles.show', ['id' => $role->id])->with('status', 'Успешно обновлена запись #' . $role->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        if (!$role->is_editable) {
            return redirect()->back()->with('error', 'Не может быть удалено');
        }

        $role->delete();

        return redirect()->route('roles.index');
    }

    private function transformPermissions(Collection $permissions)
    {
        $result = array();

        $permissions->transform(function ($item, $key) use (&$result) {
            if (!isset($result[$item->model])) {
                $result[$item->model] = collect();
            }
            $result[$item->model]->push($item);
        });

        return $result;
    }

    public function getPermissions()
    {
        $permissions = Permission::with('roles')->orderBy('model', 'ASC')->orderBy('id', 'ASC')->get();
        return $this->transformPermissions($permissions);
    }
}
