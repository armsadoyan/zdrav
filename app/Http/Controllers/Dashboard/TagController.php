<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        return view('admin.tags.index')->with('tags', Tag::paginate(20));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['slug' => 'required|unique:tags,slug', 'name' => 'required|string']);
        $tag = Tag::create($request->only(['slug', 'name']));
        return redirect()->back();
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect()->back();
    }
}
