<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\UserUpdateRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $this->authorize('user-read', \Auth::user());

        $users = User::paginate(20);

        return view('admin.user.index')->with('users', $users);
    }

    public function edit(User $user)
    {
        $this->authorize('user-update', \Auth::user());
        $roles = Role::all();
        return view('admin.user.edit')
            ->with('user', $user)
            ->with('roles', $roles);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $this->authorize('user-update', \Auth::user());
        $user->update($request->all());
        return redirect()->route('users.index')->with('status', 'Успешно обновлен пользователь #' . $user->id);
    }

    public function destroy(User $user)
    {
        $this->authorize('user-delete', \Auth::user());
        $user->delete();

        return redirect()->route('users.index')->with('status', 'Успешно удален пользователь #' . $user->id);
    }
}
