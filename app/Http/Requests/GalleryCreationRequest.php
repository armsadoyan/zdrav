<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class GalleryCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'img_name' => 'required|image|mimes:jpg,png',
        ];
    }

    public function inputs(){
        $inputs = $this->except(['_token']);

        $inputs['user_id'] = Auth::id();

        if($this->hasFile('img_name')) {
            $image = $this->file('img_name');
            $inputs['img_name'] = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('client/static/profiles/gallery/'), $inputs['img_name']);
        }

        return $inputs;
    }
}
