<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfoCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'organisation_description' => 'string',
            'stars' => 'integer',
            'payments_terms' => 'string',
            'docs' => 'string',
            'contraindications' => 'string',
            'medical_profiles' => 'string|max:191',
            'treatment_methods' => 'string|max:191',
            'services' => 'string|max:191',
        ];
    }
}