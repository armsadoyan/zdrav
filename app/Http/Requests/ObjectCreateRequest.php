<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObjectCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'url' => 'required|string|unique:places,url',
            'country_id' => 'required|exists:countries,id',
            'state_id' => 'nullable|exists:states,id',
            'city_id' => 'nullable|exists:cities,id',
            'description' => 'required|string',
            'stars' => 'required|in:1,2,3,4,5',
            'payment_conditions' => 'required|string',
            'required_documents' => 'required|string',
            'visa_information' => 'required|string',
            'contraindications' => 'required|string',
            'medical_profiles' => 'array|nullable',
            'medical_profiles.*' => 'exists:medical_profiles,id',
            'therapies' => 'array|nullable',
            'therapies.*' => 'exists:therapies,id',
            'is_published' => 'nullable'
        ];
    }
}
