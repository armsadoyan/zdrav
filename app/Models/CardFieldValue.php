<?php

namespace App\Models;

use App\Exceptions\FormException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CardFieldValue extends Model
{
    /**
     * @var array
     */
    public static $validation_message = [
        'required' => ':attribute is required',
        'email' => ':attribute is note email',
        'unique' => ':attribute already exist',
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'card_id',
        'card_type_id',
        'type',
        'value',
    ];

    /**
     * @param $data
     * @return array
     */
    public static function addOrUpdateCard($data)
    {
        try{
            $auth = Auth::user();
//            $data['user_id'] = 1;
            $data['user_id'] = $auth->id;
//            $v = Validator::make($data, [
//                'user_id' => 'required',
//                'card_id' => 'required',
//                'card_type_id' => 'required',
//                'type' => 'required',
//                'value' => 'required',
//            ],self::$validation_message);
//
//            if ($v->fails()) {
//                $status = Response::HTTP_BAD_REQUEST;
//                $response = [
//                    'status' => $status,
//                    'message' => $v->errors(),
//                ];
//                throw new FormException($response);
//            }
//            CardFieldValue::updateOrCreate(['user_id' => $data['user_id'],'card_id' => $data['card_id'],'card_type_id' => $data['card_type_id']], $data);
            $status = Response::HTTP_OK;
            return $response = [
                'status' => $status,
                'message' => $data,
            ];
            return $response;
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => $e->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * @return array
     */
    public static function getCard()
    {
        try{
            $auth = Auth::user();
            $card = CardFieldValue::where('user_id', Auth::id())->get();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'data' => $card,
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Ничего не найдено.',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => $e->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * @param array|int $id
     * @return array|int
     */
    public static function destroy($id)
    {
        try {
            $delCardFieldValue = CardFieldValue::where('user_id',$id)->delete();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'Card has been deleted successfully'
            ];
        } catch (\Exception $e) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }
}
