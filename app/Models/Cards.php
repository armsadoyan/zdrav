<?php

namespace App\Models;

use App\Exceptions\FormException;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class Cards extends Model
{


    use SoftDeletes;
    // is_published means, that this news is going to show on the main page. Elsewhere, it is visible, but not published.
    public $image;
    protected $fillable = [
        'name',
        'image_url'
    ];

    /**
     * @var array
     */
    public static $validation_message = [
        'required' => ':attribute is required',
        'email' => ':attribute is note email',
        'unique' => ':attribute already exist',
    ];

    public function file()
    {
        return $this->belongsTo(File::class);
    }

    public function getImageAttribute()
    {
        if ($this->file) {
            return $this->file->filepath;
        } else {
            return 'http://via.placeholder.com/275x183';
        }
    }

    /**
     * @param $data
     * @return array
     */
    public static function add($data)
    {
        try{
            Cards::create($data);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'News has been added successfully',
            ];
            return $response;
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => $e->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * @return array
     */
    public static function getAllCards()
    {
        $news = Cards::all();
        $status = Response::HTTP_OK;
        $response = [
            'status' => $status,
            'data' => $news,
        ];
        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public static function getCardsById($id)
    {
        try{
            $news = Cards::findOrFail($id);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'data' => $news,
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

    /**
     * @param $data
     * @param $id
     * @return array
     */
    public static function edit($data,$id)
    {
        try{
            $news = Cards::findOrFail($id);
            $news->update($data);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'data' => $news,
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

    /**
     * @param array|int $id
     * @return array|int
     */
    public static function destroy($id)
    {
        try {
            $delNews = Cards::findOrFail($id);
            $delNews->delete();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'News has been deleted successfully'

            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        } catch (\Exception $e) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }
}
