<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Desease extends Model
{
    protected $guarded = [];
    protected $hidden = ['parent_id', 'created_at', 'updated_at', 'accestors'];
    protected $appends = ['children'];

    public function therapies()
    {
        return $this->belongsToMany(Therapy::class);
    }

    public function medicalProfiles()
    {
        return $this->belongsToMany(MedicalProfile::class);
    }

    public function accestors()
    {
        return $this->hasMany(Desease::class, 'parent_id');
    }

    public function getChildrenAttribute()
    {
        return $this->accestors;
    }
}
