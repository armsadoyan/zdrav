<?php

namespace App\Models;

use App\Exceptions\FormException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class Gallery extends Model
{
    /**
     * @var array
     */
    public static $validation_message = [
        'required' => ':attribute is required',
        'unique' => ':attribute already exist',
        'array' => ':attribute is note array',
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'img_name',
        'status',
    ];

    public static function addImages($data){
//        try{
        $auth = Auth::user();
        $data = $data["gallery"];
        $data['user_id'] = Auth::id() > 0 ? Auth::id() : 1;
        $v = Validator::make($data, [],self::$validation_message);

//        if ($v->fails()) {
//            $status = Response::HTTP_BAD_REQUEST;
//            $response = [
//                'status' => $status,
//                'message' => $v->errors(),
//            ];
//            throw new FormException(json_encode($response));
//        }

        foreach ($data['images']["value"] as $img){
            $img = $img["url"];
            if(strpos($img, "base64") != false){
                $extension = explode(';', $img);
                $ini = str_replace("data:image/", "",$extension[0]);
                $img = str_replace('data:image/jpeg;base64,','',$img);
                $img = str_replace('data:image/jpg;base64,','',$img);
                $img = str_replace('data:image/png;base64,','',$img);
                $img = str_replace('data:image/bmp;base64,','',$img);
                $filename = $data['user_id']."_".rand(100, 99999999) . ".".$ini;
                if(Storage::disk('gallery')->put($filename, base64_decode($img))){
                    $data['img_name'] = $filename;
                    Gallery::create($data);
                }
            }else{
                $data['img_name'] = $img;
            }
        }
        $status = Response::HTTP_OK;
        $response = [
            'status' => 200,
            'message' => $data,
        ];
        return $response;
//        }catch (\Exception $e){
//            $status = Response::HTTP_BAD_REQUEST;
//            $response = [
//                'status' => $status,
//                'message' => 'Что-то пошло не так.',
//            ];
//        }
    }

    public static function getPhotosInModeration(){
        $photos = Gallery::where('status','in_moderation')->get();
        $status = Response::HTTP_OK;
        $response = [
            'status' => $status,
            'data' => $photos,
        ];
        return $response;
    }

    public static function changePhotoStatus($data){
        try{
            $gallery = Gallery::findOrFail($data['id']);
            $gallery->status = $data['status'];
            $gallery->save();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'Changes with the gallery were successful',
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

    public static function getMyGallery(){
        $gallery = Gallery::where('user_id', Auth::id())->get();
        $info["value"] = [];
        foreach ($gallery as $k => $item) {
            $info["value"][$k]["name"] = "Описание";
            $info["value"][$k]["url"] = !empty($item->img_name) ? "/profiles/gallery/".$item->img_name : "";
            $info["value"][$k]["status"] = $item->status;
            $info["value"][$k]["uid"] = $item->id;
            $info["value"][$k]["size"] = !empty($item->img_name) ? filesize("/var/www/client/static/profiles/gallery/".$item->img_name) : 0;

            if($info["value"][$k]["status"] == "accept"){
                $info["value"][$k]["moderated"] = "true";
            }elseif($info["value"][$k]["status"] == "delete"){
                $info["value"][$k]["moderated"] = "false";
            }else{
                unset($info["value"][$k]["moderated"]);
            }
            $info["value"][$k]["status"] = "success";
        }

        $status = Response::HTTP_OK;
        $response = [
            'status' => 200,
            'data' => array('images' => array('name' => 'Описание', 'value' => $info["value"], 'type' => 'file')),
        ];
        return $response;
    }
}
