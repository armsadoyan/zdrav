<?php

namespace App\Models;

use App\Exceptions\FormException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class Info extends Model
{
    /**
     * @var array
     */
    public static $validation_message = [
        'required' => ':attribute is required',
        'email' => ':attribute is note email',
        'unique' => ':attribute already exist',
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'organisation_description',
        'stars',
        'payments_terms',
        'docs',
        'honour',
        'contraindications',
        'medical_profiles',
        'treatment_methods',
        'services',
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    /**
     * @param $data
     * @return array
     */
    public static function createOrUpdate(Request $request, $data)
    {
        $auth = Auth::user();
        $data = $data["infoform"];
        $nagradiArr = array();
        $data['user_id'] = Auth::id();
        $data['organisation_description'] = (string)$data["description"]["value"];
        $data['stars'] = (string)$data["stars"]["value"];
        $data['payments_terms'] = (string)$data["paymentconditions"]["value"];
        $data['docs'] = (string)$data["documentslist"]["value"];
        $data['contraindications'] = (string)$data["contraindications"]["value"];
        foreach ($data["honour"]["value"] as $val) {
            $img = $val["url"];
            $extension = explode(';', $img);
            $ini = str_replace("data:image/", "",$extension[0]);
            $img = str_replace('data:image/jpeg;base64,','',$img);
            $img = str_replace('data:image/jpg;base64,','',$img);
            $img = str_replace('data:image/png;base64,','',$img);
            $img = str_replace('data:image/bmp;base64,','',$img);
            $filename = $data['user_id']."_".rand(100, 99999999) . ".".$ini;
            if(Storage::disk('nagradi')->put($filename, base64_decode($img))){
                $nagradiArr[] = $filename;
            }
        }
        $data['honour'] = implode(",", $nagradiArr);
        $data['medical_profiles'] = isset($data["medprofile"]["value"][0]) ? implode("@,@", $data["medprofile"]["value"]) : "";
        $data['treatment_methods'] = isset($data["medmethods"]["value"][0]) ? implode("@,@", $data["medmethods"]["value"]) : "";
        $data['services'] = isset($data["solutions"]["value"][0]) ? implode("@,@", $data["solutions"]["value"]) : "";

//        if(!empty($data["honour"]["value"])){
//            $imgInfo = $data["honour"];
//            $imagesArr = $request->input('profileform.avatar.value');
//            foreach ($imagesArr as $img) {
//                $imgInfo = $img["url"];
//                $imgInfo = str_replace('data:image/jpeg;base64,','',$imgInfo);
//                $imgInfo = str_replace('data:image/jpg;base64,','',$imgInfo);
//                $imgInfo = str_replace('data:image/png;base64,','',$imgInfo);
//                $imgInfo = str_replace('data:image/bmp;base64,','',$imgInfo);
//                $img = explode(',', $imgInfo);
//                $ini = substr($img[0], 11);
//                $type = explode(';', $ini);
//                $filename = $user->id.rand(1000, 9999999999).'.'.$type;
//                if(Storage::disk('nagradi')->put($filename, base64_decode($imgInfo))){
//                    $data['honour'][] = $filename;
//                }
//            }
//            $data['honour'] = implode(",", $data['honour']);
//        }
//            $data['user_id'] = 1;
//        try{
//            $v = Validator::make($data, [
//                'organisation_description' => 'required',
//                'stars' => 'required',
//                'payments_terms' => 'required',
//                'docs' => 'required',
//                'contraindications' => 'required',
//                'medical_profiles' => 'required',
//                'treatment_methods' => 'required',
//                'services' => 'required',
//            ],self::$validation_message);

//            if ($v->fails()) {
//                $status = Response::HTTP_BAD_REQUEST;
//                $response = [
//                    'status' => $status,
//                    'message' => $v->errors(),
//                ];
//                throw new FormException($response);
//            }
//                return $response = [
//                        'status' => 200,
//                        'message' => $data,
//                    ];
            $info = Info::where("user_id", $data["user_id"])->first();

            if(empty($info)){
                Info::create($data);

                $status = Response::HTTP_OK;
                $response = [
                    'status' => $status,
                    'message' => 'Информация успешно сохранена.',
                ];
            }else{
                $data['info_id'] = $info->id;
                $infoObj = new Info();
                if($info->update($data)){
                    $response = [
                        'status' => Response::HTTP_OK,
                        'message' => 'Информация успешно обновлена.',
                    ];
                }
            }

            return $response;
//        }catch (\Exception $e){
//            $status = Response::HTTP_BAD_REQUEST;
//            $response = [
//                'status' => $status,
//                'message' => 'Что-то пошло не так.',
//            ];
//        }
//        return $response;
    }

    /**
     * @return array
     */
    public static function getInfo($id)
    {
        if(empty($id)){
            $auth = Auth::user();
            $news = Info::where('user_id', Auth::id())->first();
            if(empty($news)){
                $status = Response::HTTP_BAD_REQUEST;
                $response = [
                    'status' => $status,
                    'message' => 'Ничего не найдено.',
                ];
            }else{
                $i = 0;
                $news->original['description'] = $news->original["organisation_description"];
                $news->original['payments_terms'] = $news->original["paymentconditions"];
                $news->original['documentslist'] = $news->original["docs"];
                $news->original['medprofile'] = $news->original["medical_profiles"];
                $news->original['medmethods'] = $news->original["treatment_methods"];
                $news->original['solutions'] = $news->original["services"];
                foreach ($news->original as $k => $item) {
                    $moderate = 0;
                    if($k == "status"){
                        if($item == "accept"){
                            $moderated = "true";
                        }elseif($item == "declined"){
                            $moderated = "false";
                        }
                    }
                    if($k == "honour"){
                        $info[$k]["name"] = $k;
                        $info[$k]["type"] = "file";
                        $info[$k]["value"][$i]["url"] = !empty($item) ? "/profiles/nagradi/".$item : "";
                        $info[$k]["value"][$i]["uid"] = rand(100, 999999);
                        $info[$k]["value"][$i]["size"] = !empty($item) ? filesize("/var/www/client/static/profiles/nagradi/".$item) : 0;
                        if($moderate != 0){
                            $info[$k]["moderate"] = $moderate;
                        }
                        $i++;
                    }elseif(strpos($item, "@,@") !== false){
                        $info[$k]["name"] = $k;
                        if($moderate != 0){
                            $info[$k]["moderate"] = $moderate;
                        }
                        $info[$k]["type"] = "array";
                        $info[$k]["value"] = $item;
                    }else{
                        $info[$k]["name"] = $k;
                        if($moderate != 0){
                            $info[$k]["moderate"] = $moderate;
                        }
                        $info[$k]["type"] = "string";
                        $info[$k]["value"] = $item;
                    }
                }
                $status = Response::HTTP_OK;
                $response = [
                    'status' => $status,
                    'data' => $info,
                ];
            }
        }else{
            try{
                $news = Info::where('id', $id)->firstOrFail();
                $status = Response::HTTP_OK;
                $response = [
                    'status' => $status,
                    'data' => $news,
                ];
            }catch (ModelNotFoundException $e){
                $status = Response::HTTP_BAD_REQUEST;
                $response = [
                    'status' => $status,
                    'message' => 'Ничего не найдено.',
                ];
            }catch (\Exception $e){
                $status = Response::HTTP_BAD_REQUEST;
                $response = [
                    'status' => $status,
                    'message' => 'Что-то пошло не так.',
                ];
            }
        }

        return $response;
    }

    /**
     * @param array|int $id
     * @return array|int
     */
    public static function destroy($id)
    {
        try {
            $delInfo = Info::findOrFail($id);
            $delInfo->delete();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'Информация удалена.'
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Ничего не найдено.',
            ];
        } catch (\Exception $e) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Что-то пошло не так.',
            ];
        }
        return $response;
    }
}
