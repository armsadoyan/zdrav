<?php

namespace App\Models;

use App\Exceptions\FormException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class InfoModeration extends Model
{
    /**
     * @var array
     */
    public static $validation_message = [
        'required' => ':attribute is required',
        'email' => ':attribute is note email',
        'unique' => ':attribute already exist',
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'info_id',
        'user_id',
        'organisation_description',
        'stars',
        'payments_terms',
        'docs',
        'contraindications',
        'honour',
        'medical_profiles',
        'treatment_methods',
        'services',
    ];

    /**
     * @param $data
     * @return array
     */
    public static function add($data)
    {
        try{
            $auth = Auth::user();
          $data['user_id'] = $auth->id;
//            $data['user_id'] = 1;
//            $v = Validator::make($data, [
//                'info_id' => 'required',
//                'organisation_description' => 'required',
//                'stars' => 'required',
//                'payments_terms' => 'required',
//                'docs' => 'required',
//                'contraindications' => 'required',
//                'medical_profiles' => 'required',
//                'treatment_methods' => 'required',
//                'services' => 'required',
//            ],self::$validation_message);
//
//            if ($v->fails()) {
//                $status = Response::HTTP_BAD_REQUEST;
//                $response = [
//                    'status' => $status,
//                    'message' => $v->errors(),
//                ];
//                throw new FormException($response);
//            }
            InfoModeration::create($data);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'Информация отправлена на модерацию.',
            ];
            return $response;
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Что-то пошло не так.',
            ];
        }
        return $response;
    }

    public static function acceptOrDelete($data){
        try{
            $infoModeration = InfoModeration::where('info_id',$data['info_id'])->firstOrFail();
            if($data['status'] == 'accept'){
                $info = Info::find($infoModeration->info_id);
                $info->update($infoModeration->toArray());
            }
            $infoModeration->delete();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'Changes with the information were successful',
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }

        return $response;
    }
}
