<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MedicalProfile
 * @package App\Models
 *
 * @method static findOrFail(int $id)
 */
class MedicalProfile extends Model
{
    protected $guarded = ['id'];
    public function deseases()
    {
        return $this->belongsToMany(Desease::class);
    }
}
