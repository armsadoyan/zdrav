<?php

namespace App\Models;

use App\Exceptions\FormException;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class News extends Model
{
    public $image;

    use SoftDeletes;
    // is_published means, that this news is going to show on the main page. Elsewhere, it is visible, but not published.

    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keywords',
        'short_text',
        'full_text',
        'is_visible',
        'is_published',
        'file_id',
        'title',
        'date',
        'picture'
    ];

    /**
     * @var array
     */
    public static $validation_message = [
        'required' => ':attribute is required',
        'email' => ':attribute is note email',
        'unique' => ':attribute already exist',
    ];

//    protected $hidden = ['is_visible', 'is_published'];
//
//    protected $appends = ['image'];
//
//    protected $dates = ['created_at', 'updated_at', 'date', 'deleted_at'];

    public function file()
    {
        return $this->belongsTo(File::class);
    }

    public function getImageAttribute()
    {
        if ($this->file) {
           return $this->file->filepath;
        } else {
            return 'http://via.placeholder.com/275x183';
        }
    }

    /**
     * @param $data
     * @return array
     */
    public static function add($data)
    {
        try{
            $v = Validator::make($data, [
                'meta_title' => 'required',
                'meta_description' => 'required',
                'meta_keywords' => 'required',
                'short_text' => 'required',
                'full_text' => 'required',
                'title' => 'required',
                'date' => 'required',
            ],self::$validation_message);

            if ($v->fails()) {
                $status = Response::HTTP_BAD_REQUEST;
                $response = [
                    'status' => $status,
                    'message' => $v->errors(),
                ];
                throw new FormException($response);
            }
            News::create($data);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'News has been added successfully',
            ];
            return $response;
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => $e->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * @return array
     */
    public static function getAllNews()
    {
        $news = News::all();
        $status = Response::HTTP_OK;
        $response = [
            'status' => $status,
            'data' => $news,
        ];
        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public static function getNewsById($id)
    {
        try{
            $news = News::findOrFail($id);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'data' => $news,
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

    /**
     * @param $data
     * @param $id
     * @return array
     */
    public static function edit($data,$id)
    {
        try{
            $news = News::findOrFail($id);
            $news->update($data);
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'data' => $news,
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

    /**
     * @param array|int $id
     * @return array|int
     */
    public static function destroy($id)
    {
        try {
            $delNews = News::findOrFail($id);
            $delNews->delete();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'News has been deleted successfully'

            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        } catch (\Exception $e) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }
}
