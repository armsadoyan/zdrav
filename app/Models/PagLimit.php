<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagLimit extends Model
{
    protected $table = 'pagination_limit';
    protected $fillable = [
        'pag_limit', 'user_id'
    ];
}
