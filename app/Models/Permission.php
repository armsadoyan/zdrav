<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'name',
        'model',
        'slug',
        'has_default'
    ];
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

}
