<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function places()
    {
        return $this->belongsToMany(Place::class);
    }
}
