<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Place extends Model
{
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('published', function (Builder $builder) {
            $builder->where('is_published', true);
        });
    }

    public function files()
    {
        return $this->belongsToMany(File::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function type()
    {
        return $this->belongsTo(PlaceType::class, 'place_type_id');
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }

    public function medicalProfiles()
    {
        return $this->belongsToMany(MedicalProfile::class);
    }

    public function therapies()
    {
        return $this->belongsToMany(Therapy::class);
    }
}
