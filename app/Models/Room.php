<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}
