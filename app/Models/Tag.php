<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];
    public function faqs()
    {
        return $this->belongsToMany(Faq::class);
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
