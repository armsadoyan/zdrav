<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Therapy extends Model
{
    protected $guarded = ['id'];
    public function deseases()
    {
        return $this->belongsToMany(Desease::class);
    }
}
