<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $permissionsList;

    //public $appends = ['image'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'father_name',
        'last_name',
        'city',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class)->with('permissions');
    }

    public function permissions()
    {
        if (!$this->permissionsList) {
            $this->permissionsList = $this->role ? $this->role->permissions : Permission::where('has_default',
                true)->get();
        }

        return $this->permissionsList;
    }

    public function hasPermission(string $permission)
    {
        return $this->permissions()->where('slug', $permission)->count();
    }

    /*public function getImageAttribute()
    {
        $url = $this->avatar_url ?? 'public/images/avatar.svg';
        return \Storage::url($url);
    }*/

    /*public function getAvatarUrlAttribute($avatar)
    {
        return \Storage::url($avatar);
    }*/
}
