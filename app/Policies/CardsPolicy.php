<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CardsPolicy
{
    use HandlesAuthorization;

    public function cardsCreate(User $user)
    {
        return $user->hasPermission('cards-create');
    }

    public function cardsRead(User $user)
    {
        return true;
    }

    public function cardsUpdate(User $user)
    {
        return $user->hasPermission('cards-update');
    }

    public function cardsDelete(User $user)
    {
        return $user->hasPermission('cards-delete');
    }
}
