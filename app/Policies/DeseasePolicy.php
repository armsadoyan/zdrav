<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeseasePolicy
{
    use HandlesAuthorization;

    public function deseaseCreate (User $user)
    {
        return $user->hasPermission('desease-create');
    }

    public function deseaseRead (User $user)
    {
        return $user->hasPermission('desease-read');
    }

    public function deseaseUpdate (User $user)
    {
        return $user->hasPermission('desease-update');
    }

    public function deseaseDelete (User $user)
    {
        return $user->hasPermission('desease-delete');
    }
}
