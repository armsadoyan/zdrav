<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GalleriesPolicy
{
    use HandlesAuthorization;

    public function galleryCreate(User $user)
    {
        return $user->hasPermission('gallery-create');
    }

    public function galleryRead(User $user)
    {
        return true;
    }

    public function galleryUpdate(User $user)
    {
        return $user->hasPermission('gallery-update');
    }

    public function galleryDelete(User $user)
    {
        return $user->hasPermission('gallery-delete');
    }
}
