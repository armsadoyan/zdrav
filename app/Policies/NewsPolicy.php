<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    public function newsCreate(User $user)
    {
        return $user->hasPermission('news-create');
    }

    public function newsRead(User $user)
    {
        return true;
    }

    public function newsUpdate(User $user)
    {
        return $user->hasPermission('news-update');
    }

    public function newsDelete(User $user)
    {
        return $user->hasPermission('news-delete');
    }
}
