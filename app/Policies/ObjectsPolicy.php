<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ObjectsPolicy
{
    use HandlesAuthorization;

    public function objectCreate(User $user)
    {
        return $user->hasPermission('object-create');
    }

    public function objectRead(User $user)
    {
        return true;
    }

    public function objectUpdate(User $user)
    {
        return $user->hasPermission('object-update');
    }

    public function objectDelete(User $user)
    {
        return $user->hasPermission('object-delete');
    }
}
