<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function roleCreate(User $user)
    {
        return $user->hasPermission('role-create');
    }

    public function roleRead(User $user)
    {
        return $user->hasPermission('role-read');
    }

    public function roleUpdate(User $user)
    {
        return $user->hasPermission('role-update');
    }

    public function roleDelete(User $user)
    {
        return $user->hasPermission('role-delete');
    }
}
