<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function userCreate (User $user)
    {
        return $user->hasPermission('user-create');
    }

    public function userRead (User $user)
    {
        return $user->hasPermission('user-read');
    }

    public function userUpdate (User $user)
    {
        return $user->hasPermission('user-update');
    }

    public function userDelete (User $user)
    {
        return $user->hasPermission('user-delete');
    }

}
