<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\Passport;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @param GateContract $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        Route::middleware(\Barryvdh\Cors\HandleCors::class)->group(function () {
            Passport::routes();
        });

        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));

        Passport::enableImplicitGrant();

        foreach ($this->getPermissions() as $permission) {
            $gate->define($permission->slug, 'App\Policies\\' . $permission->model . 'Policy@' . camel_case($permission->slug));
        }

        \View::composer('*', function ($view) {
            $view->with('currentUser', \Auth::user());
        });
    }

    protected function getPermissions()
    {
        //Fixing bug, when we cannot use php artisan migrate because of trying to get data from unexisting sources
        if(Schema::hasTable('permissions')){
            return Permission::all();
        };

        return [];
    }
}
