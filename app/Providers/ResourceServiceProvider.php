<?php

namespace App\Providers;

use App\Contracts\DeseaseInterface;
use App\Contracts\MedicalProfileInterface;
use App\Contracts\NewsInterface;
use App\Contracts\ObjectInterface;
use App\Contracts\TherapyInterface;
use App\Resources\Eloquent\Desease;
use App\Resources\Eloquent\MedicalProfile;
use App\Resources\Eloquent\News;
use App\Resources\Eloquent\Cards;
//use App\Resources\Eloquent\Object;
use App\Resources\Eloquent\Therapy;
use Illuminate\Support\ServiceProvider;

class ResourceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ObjectInterface::class, function () {
            return new Object();
        });

        $this->app->bind(NewsInterface::class, function () {
           return new News();
        });
        $this->app->bind(CardsInterface::class, function () {
           return new Cards();
        });
        $this->app->bind(DeseaseInterface::class, function () {
           return new Desease();
        });
        $this->app->bind(MedicalProfileInterface::class, function () {
           return new MedicalProfile();
        });
        $this->app->bind(TherapyInterface::class, function () {
           return new Therapy();
        });
    }
}
