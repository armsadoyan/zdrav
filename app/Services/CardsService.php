<?php

namespace App\Services;

use App\Models\Cards;
use App\Models\File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class CardsService
{
    public function all() : Collection
    {
        return Cards::get();
    }

    public function getById($id) : Model
    {
        return Cards::findOrFail($id);
    }

    public function getWithPagination()
    {
        return Cards::paginate(20);
    }

    public function store(array $attributes)
    {
        /*
         TODO: Storage::disk => change public to news
         TODO: in config/filesystem.php fix 'news' path
         TODO: integrate file upload logic in update news
         */
        $fileName = "";
        if(isset($attributes['image'])){
            $fileName = "anketas_".rand(10, 9999999).'.png';
            $file = Input::file('image');
            $file->move("/var/www/client/static/profiles/anketas/", $fileName);
        }

        $data = [
            'name' => $attributes['name'],
            'image_url' => !empty($fileName) ? $fileName : NULL,
        ];

        $cards = Cards::create($data);

        return $cards;
    }

    public function update(array $attributes, $id)
    {
        $cards = Cards::findOrFail($id)->update([
            'name' => $attributes['name'],
            'image_url' => $attributes['image'],
        ]);

        return $cards;
    }

    public function delete($id)
    {
//        Cards::findOrFail($id)->delete();
    }
}