<?php

namespace App\Services;

use App\Models\Desease;
use Illuminate\Support\Collection;

class DeseaseService
{
    public function all() : Collection
    {
        return Desease::with(['therapies', 'medicalProfiles'])->get();
    }
}