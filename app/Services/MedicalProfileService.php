<?php

namespace App\Services;


use App\Models\MedicalProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class MedicalProfileService
{
    public static function getById($id) : Model
    {
        return MedicalProfile::findOrFail($id);
    }

    public function all() : Collection
    {
        return MedicalProfile::all();
    }
}