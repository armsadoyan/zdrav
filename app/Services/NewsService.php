<?php

namespace App\Services;

use App\Models\File;
use App\Models\News;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class NewsService
{
    public function all() : Collection
    {
        return News::with('file')
            ->where('is_visible', true)
            ->get();
    }

    public function getById($id) : Model
    {
        return News::where('is_visible', true)
            ->with('file')
            ->findOrFail($id);
    }

    public function getWithPagination()
    {
        return News::paginate(20);
    }

    public function store(array $attributes)
    {
        /*
         TODO: Storage::disk => change public to news
         TODO: in config/filesystem.php fix 'news' path
         TODO: integrate file upload logic in update news
         */
        if(isset($attributes['image'])){
            $fileName = "news_".rand(10, 9999999).'.png';
            $file = Input::file('image');
            $file->move("/var/www/client/static/news/", $fileName);
//            $saveFile = Storage::disk('news')->put($fileName, $attributes['image']);
//
//            if ($saveFile) {
//                $fileArr = explode('/', Storage::disk('news')->url($fileName));
//
//                $file = File::create([
//                    'path' => str_replace($fileArr[count($fileArr)-1], '', Storage::disk('news')->url($fileName)),
//                    'original_name' => $fileArr[count($fileArr)-1],
//                    'type' => 'photo',
//                    'status' => 'visible'
//                ]);
//
//                if ($file) $fileId = $file->id;
//            }
        }

        $data = [
            'file_id' => null,
            'meta_title' => $attributes['title'],
            'meta_description' => $attributes['meta_description'],
            'meta_keywords' => $attributes['meta_keywords'],
            'short_text' => $attributes['short_text'],
            'full_text' => $attributes['full_text'],
            'is_visible' => isset($attributes['is_visible']),
            'is_published' => isset($attributes['is_published']),
            'title' => $attributes['title'],
            'date' => Carbon::parse($attributes['date']),
            'picture' => isset($fileName) ? $fileName : '',
        ];

        $news = News::create($data);

        return $news;
    }

    public function update(array $attributes, $id)
    {
        $news = News::findOrFail($id)->update([
            'meta_title' => $attributes['title'],
            'meta_description' => $attributes['meta_description'],
            'meta_keywords' => $attributes['meta_keywords'],
            'short_text' => $attributes['short_text'],
            'full_text' => $attributes['full_text'],
            'is_visible' => isset($attributes['is_visible']),
            'is_published' => isset($attributes['is_published']),
            'title' => $attributes['title'],
            'date' => Carbon::parse($attributes['date'])
        ]);

        return $news;
    }

    public function delete($id)
    {
        News::findOrFail($id)->delete();
    }
}