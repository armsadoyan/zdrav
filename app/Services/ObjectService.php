<?php

namespace App\Services;

use App\Models\Place;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ObjectService
{
    public function all() : Collection
    {
        $places = Place::with(['type', 'files', 'rooms'])->get();
        return $places;
    }

    public function getById(int $id) : Model
    {
        $place = Place::with(['type', 'files', 'rooms'])->findOrFail($id);
        return $place;
    }


}