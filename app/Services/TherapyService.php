<?php

namespace App\Services;

use App\Models\Therapy;
use Illuminate\Support\Collection;

class TherapyService
{
    public function all() : Collection
    {
        return Therapy::with('deseases')->get();
    }
}