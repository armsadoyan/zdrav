require("dotenv").config();

module.exports = {
    performance: {
        gzip: true
    },
    generate: {
        dir: '../public/_nuxt'
    },
    head: {
        title: process.env.APP_NAME,
        titleTemplate: '%s - ' + process.env.APP_NAME,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.APP_DESCRIPTION }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700;subset=cyrillic|Material+Icons' },
            { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' },
        ]
    },
    plugins: [
        '~/plugins/vuetify.js',
        '~/plugins/elements.js',
        '~/plugins/vueeditor.js',
        '~/plugins/vuecroppa.js',
        { src: '~/plugins/swiper.js', ssr: false },
        '~/plugins/youtube.js',
        '~/plugins/axios.js',
        '~/plugins/eventbus.js'
    ],
    modules: ["@nuxtjs/axios", "@nuxtjs/auth", "@nuxtjs/dotenv"],
    axios: {
        baseURL: process.env.LARAVEL_ENDPOINT,
        withCredentials: true
    },
    auth: {
        redirect: {
            login: "/login",
            logout: "/",
            callback: "/login",
            home: "/lk/profile/"
        },
        resetOnError: true,
        fullPathRedirect: true,
        rewriteRedirects: true,
        strategies: {
            password_grant: {
                _scheme: "local",
                endpoints: {
                    login: {
                        url: "/oauth/token",
                        method: "post",
                        propertyName: "access_token"
                    },
                    logout: false,
                    user: {
                        url: "api/me"
                    }
                }
            },

            'laravel.passport': {
                url: process.env.LARAVEL_ENDPOINT,
                client_id: process.env.PASSPORT_CLIENT_ID,
                client_secret: process.env.PASSPORT_CLIENT_SECRET,
                userinfo_endpoint: process.env.LARAVEL_ENDPOINT + "/api/me",
            }
        }
    },

    css: [{
            src: '~/assets/style/vuetify.styl',
            lang: 'styl'
        },
        {
            src: '~node_modules/element-ui/lib/theme-chalk/index.css',
            lang: 'css'
        },
        {
            src: '~/assets/style/custom.css',
            lang: 'css'
        },
        {
            src: '~node_modules/swiper/dist/css/swiper.min.css',
            lang: 'css'
        },

    ],

    loading: { color: '#3B8070' },
    build: {

        vendor: [
            '~/plugins/vuetify.js',
            '~/plugins/elements.js',
            '~/plugins/vueeditor.js',
            '~/plugins/vuecroppa.js',
            '~/plugins/youtube.js',
            '~/plugins/axios.js',
            '~/plugins/eventbus.js'
        ],
        extractCSS: true,

        extend(config, ctx) {
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    }
}