import Vue from 'vue'
const ElementUI = require('element-ui')
const locale = require('element-ui/lib/locale/lang/ru-RU')
Vue.use(ElementUI, { locale })

