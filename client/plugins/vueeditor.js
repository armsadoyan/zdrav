import Vue from 'vue'

import wysiwyg from "vue-wysiwyg";
import "vue-wysiwyg/dist/vueWysiwyg.css";
Vue.use(wysiwyg, {
    hideModules: { "headings": true, "removeFormat": true, "table": true, "code": true, "link": true, "separator": true, "image": true },
    forcePlainTextOnPaste: true
});