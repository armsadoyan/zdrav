export const state = () => ({

                avatar: { name: 'Аватар', type: 'file', value: [{ name: 'Аватар', url: '/images/avatar2.svg' }] },
                contactname: { name: 'ФИО контактного лица', type: 'string', value: ''},
                surename: { name: 'Фамилия', type: 'string', value: '' },
                name: { name: 'Имя', type: 'string', value: '' },
                fathername: { name: 'Отчество', type: 'string', value: '' },
                oldpassword: { name: 'Текущий пароль', type: 'string', value: '' },
                password: { name: 'Новый пароль', type: 'string', value: '' }
       
})