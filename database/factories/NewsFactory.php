<?php

use Faker\Generator as Faker;

$factory->define(App\Models\News::class, function (Faker $faker) {
    $title = $faker->sentence(7, true);
    $visible = $faker->boolean();
    return [
        'title' => $title,
        'meta_description' => $faker->sentence(15, true),
        'meta_keywords' => $faker->words(18, true),
        'meta_title' => $title,
        'short_text' => $faker->paragraph(5, true),
        'full_text' => $faker->realText(800),
        'date' => $faker->date('Y-m-d'),
        'is_visible' => $visible,
        'is_published' => $visible ? $faker->boolean : false,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
