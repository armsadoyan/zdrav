<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Place::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'stars' => $faker->numberBetween(1,5),
        'create_mode' => $faker->randomElement(['manual', 'automatic']),
        'latitude' => $faker->randomFloat(6, 0, 180),
        'longitude' => $faker->randomFloat(6, 0, 180),
        'address' => $faker->address,
        'description' => $faker->paragraphs(3, true),
        'rating' => $faker->numberBetween(0, 20),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
    ];
});
