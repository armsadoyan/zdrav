<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Somewhat like code = en_US, name = American English. Code may be like ru, en_us, enus, etc. Up to 5 symbols;
        Schema::create('languages', function (Blueprint $table) {
            $table->string('code', 5);
            $table->string('name');

            $table->primary('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
