<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeseaseMedicalProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desease_medical_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medical_profile_id');
            $table->unsignedInteger('desease_id');
            $table->timestamps();

            $table->unique(['medical_profile_id', 'desease_id']);
            $table->foreign('medical_profile_id')->references('id')->on('medical_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('desease_id')->references('id')->on('deseases')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desease_medical_profile');
    }
}
