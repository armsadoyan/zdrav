<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeseaseTherapyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desease_therapy', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('therapy_id');
            $table->unsignedInteger('desease_id');
            $table->timestamps();

            $table->unique(['therapy_id', 'desease_id']);
            $table->foreign('therapy_id')->references('id')->on('therapies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('desease_id')->references('id')->on('deseases')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desease_therapy');
    }
}
