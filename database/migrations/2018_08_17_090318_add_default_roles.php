<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Role::create([
            'name' => 'Администратор',
            'slug' => 'admin',
            'is_editable' => false
        ]);

        \App\Models\Role::create([
            'name' => 'Пользователь',
            'slug' => 'user',
            'is_editable' => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
