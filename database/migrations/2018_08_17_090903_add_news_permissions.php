<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $create = \App\Models\Permission::create([
            'name' => 'Создание новости',
            'slug' => 'news-create',
            'model' => 'News'
        ]);

        $read = \App\Models\Permission::create([
            'name' => 'Просмотр новости',
            'slug' => 'news-read',
            'model' => 'News',
            'has_default' => true
        ]);

        $update = \App\Models\Permission::create([
            'name' => 'Обновление новости',
            'slug' => 'news-update',
            'model' => 'News'
        ]);

        $delete = \App\Models\Permission::create([
            'name' => 'Удаление новости',
            'slug' => 'news-delete',
            'model' => 'News'
        ]);

        \App\Models\Role::where('slug', 'admin')->first()->permissions()->attach([$create->id, $update->id, $read->id, $delete->id]);

        \App\Models\Role::where('slug', 'user')->first()->permissions()->attach([$read->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
