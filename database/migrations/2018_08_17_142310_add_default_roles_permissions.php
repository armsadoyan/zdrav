<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultRolesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $create = \App\Models\Permission::create([
            'name' => 'Создание роли',
            'slug' => 'role-create',
            'model' => 'Role'
        ]);

        $read = \App\Models\Permission::create([
            'name' => 'Просмотр роли',
            'slug' => 'role-read',
            'model' => 'Role'
        ]);

        $update = \App\Models\Permission::create([
            'name' => 'Обновление роли',
            'slug' => 'role-update',
            'model' => 'Role'
        ]);

        $delete = \App\Models\Permission::create([
            'name' => 'Удаление роли',
            'slug' => 'role-delete',
            'model' => 'Role'
        ]);

        \App\Models\Role::where('slug', 'admin')->first()->permissions()->attach([$create->id, $update->id, $read->id, $delete->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
