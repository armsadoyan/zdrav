<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $create = \App\Models\Permission::create([
            'name' => 'Создание пользователя',
            'slug' => 'user-create',
            'model' => 'User'
        ]);

        $read = \App\Models\Permission::create([
            'name' => 'Просмотр пользователя',
            'slug' => 'user-read',
            'model' => 'User'
        ]);

        $update = \App\Models\Permission::create([
            'name' => 'Обновление пользователя',
            'slug' => 'user-update',
            'model' => 'User'
        ]);

        $delete = \App\Models\Permission::create([
            'name' => 'Удаление пользователя',
            'slug' => 'user-delete',
            'model' => 'User'
        ]);

        \App\Models\Role::where('slug', 'admin')->first()->permissions()->attach([$create->id, $update->id, $read->id, $delete->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
