<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeaseasePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $create = \App\Models\Permission::create([
            'name' => 'Создание заболевания',
            'slug' => 'desease-create',
            'model' => 'Desease'
        ]);

        $read = \App\Models\Permission::create([
            'name' => 'Просмотр заболевания',
            'slug' => 'desease-read',
            'model' => 'Desease',
            'has_default' => true
        ]);

        $update = \App\Models\Permission::create([
            'name' => 'Обновление заболевания',
            'slug' => 'desease-update',
            'model' => 'Desease'
        ]);

        $delete = \App\Models\Permission::create([
            'name' => 'Удаление заболевания',
            'slug' => 'desease-delete',
            'model' => 'Desease'
        ]);

        \App\Models\Role::where('slug', 'admin')->first()->permissions()->attach([$create->id, $update->id, $read->id, $delete->id]);
        \App\Models\Role::where('slug', 'user')->first()->permissions()->attach([$read->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
