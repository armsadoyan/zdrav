<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedDeseasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $deseases = file_get_contents(__DIR__ . '/../dumps/deseases.sql');
        $medicalProfiles = file_get_contents(__DIR__ . '/../dumps/medical_profiles.sql');
        $therapies = file_get_contents(__DIR__ . '/../dumps/therapies.sql');

        $deseaseTherapy = file_get_contents(__DIR__ . '/../dumps/desease_therapy.sql');
        $deseaseMedicalProfile = file_get_contents(__DIR__ . '/../dumps/desease_medical_profile.sql');

        DB::unprepared('SET FOREIGN_KEY_CHECKS=0;');
        DB::beginTransaction();
        try {

            DB::table('deseases')->truncate();
            DB::table('therapies')->truncate();
            DB::table('medical_profiles')->truncate();

            DB::update('ALTER TABLE deseases AUTO_INCREMENT = 1;');
            DB::update('ALTER TABLE medical_profiles AUTO_INCREMENT = 1;');
            DB::update('ALTER TABLE therapies AUTO_INCREMENT = 1;');

            DB::unprepared($deseases);
            DB::unprepared($medicalProfiles);
            DB::unprepared($therapies);

            DB::unprepared($deseases);
            DB::unprepared($deseaseMedicalProfile);
            DB::unprepared($deseaseTherapy);

        } catch (\Exception $e) {
            print($e->getMessage());
            DB::rollBack();
            DB::unprepared('SET FOREIGN_KEY_CHECKS=1;');
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
