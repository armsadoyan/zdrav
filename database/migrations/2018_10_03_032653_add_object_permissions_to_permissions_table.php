<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObjectPermissionsToPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $create = \App\Models\Permission::create([
                'name' => 'Создание объекта',
                'slug' => 'object-create',
                'model' => 'Objects'
            ]);

            $read = \App\Models\Permission::create([
                'name' => 'Просмотр объекта',
                'slug' => 'object-read',
                'model' => 'Objects',
                'has_default' => true
            ]);

            $update = \App\Models\Permission::create([
                'name' => 'Обновление объекта',
                'slug' => 'object-update',
                'model' => 'Objects'
            ]);

            $delete = \App\Models\Permission::create([
                'name' => 'Удаление объекта',
                'slug' => 'object-delete',
                'model' => 'Objects'
            ]);

            \App\Models\Role::where('slug', 'admin')->first()->permissions()->attach([$create->id, $update->id, $read->id, $delete->id]);
            \App\Models\Role::where('slug', 'user')->first()->permissions()->attach([$read->id]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}
