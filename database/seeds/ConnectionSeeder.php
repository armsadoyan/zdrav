<?php

use App\Models\Desease;
use App\Models\MedicalProfile;
use App\Models\Therapy;
use Illuminate\Database\Seeder;

class ConnectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (MedicalProfile::first())->deseases()->attach(Desease::first()->id);
        (Desease::first())->therapies()->attach(Therapy::first()->id);
    }
}
