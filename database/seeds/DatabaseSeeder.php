<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(NewsSeeder::class);
//        $this->call(DeseaseSeeder::class);
//        $this->call(MedicalProfileSeeder::class);
//        $this->call(TherapySeeder::class);
        $this->call(ConnectionSeeder::class);
    }
}
