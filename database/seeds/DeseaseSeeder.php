<?php

use Illuminate\Database\Seeder;

class DeseaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Desease::create([
            'name' => 'НЕКОТОРЫЕ ИНФЕКЦИОННЫЕ И ПАРАЗИТАРНЫЕ БОЛЕЗНИ',
        ]);

        \App\Models\Desease::create([
            'name' => 'Холера',
        ]);
        \App\Models\Desease::create([
            'name' => 'Тиф и паратиф',
        ]);
        \App\Models\Desease::create([
            'name' => 'Паратиф А',
        ]);
        \App\Models\Desease::create([
            'name' => 'Паратиф B',
        ]);
        \App\Models\Desease::create([
            'name' => 'Паратиф С',
        ]);
        \App\Models\Desease::create([
            'name' => 'Паратиф D',
        ]);
        \App\Models\Desease::create([
            'name' => 'Паратиф неуточненный',
        ]);
        \App\Models\Desease::create([
            'name' => 'Другие неуточненные инфекции',
        ]);
    }
}
