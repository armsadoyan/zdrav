<?php

use App\Models\MedicalProfile;
use Illuminate\Database\Seeder;

class MedicalProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MedicalProfile::create(['name' => 'Болезни нервной системы']);
        MedicalProfile::create(['name' => 'Болезни эндокринной системы и обмена веществ']);
        MedicalProfile::create(['name' => 'Болезни органов зрения']);
        MedicalProfile::create(['name' => 'Болезни системы кровообращения']);
        MedicalProfile::create(['name' => 'Болезни органов дыхания']);
        MedicalProfile::create(['name' => 'Болезни желудочно-кишечного тракта']);
        MedicalProfile::create(['name' => 'Болезни кожи']);
        MedicalProfile::create(['name' => 'Болезни костно-мышечной системы']);
        MedicalProfile::create(['name' => 'Урология и Гинекология']);
        MedicalProfile::create(['name' => 'Болезни уха, горла и носа']);
        MedicalProfile::create(['name' => 'Болезни опорно-двигательного аппарата']);
        MedicalProfile::create(['name' => 'Болезни сердечно-сосудистой системы']);
        MedicalProfile::create(['name' => 'Реабилитация онкологических заболеваний']);
        MedicalProfile::create(['name' => 'Стоматологические заболевания']);
    }
}
