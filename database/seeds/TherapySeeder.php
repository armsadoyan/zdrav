<?php

use Illuminate\Database\Seeder;

class TherapySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Therapy::create(['name' => 'Сбор анамнеза и жалоб общетерапевтический']);
        \App\Models\Therapy::create(['name' => 'Визуальный осмотр общетерапевтический']);
        \App\Models\Therapy::create(['name' => 'Пальпация общетерапевтическая']);
        \App\Models\Therapy::create(['name' => 'Аускультация общетерапевтическая']);
        \App\Models\Therapy::create(['name' => 'Перкуссия общетерапевтическая']);
        \App\Models\Therapy::create(['name' => 'Термометрия общая']);
        \App\Models\Therapy::create(['name' => 'Измерение роста']);
        \App\Models\Therapy::create(['name' => 'Измерение массы тела']);
        \App\Models\Therapy::create(['name' => 'Измерения частоты дыхания']);
        \App\Models\Therapy::create(['name' => 'Измерение частоты сердцебиения']);
    }
}