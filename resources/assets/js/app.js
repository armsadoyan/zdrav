
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });

require('selectize');
require('selectize/dist/css/selectize.css');
$('.js-selectize').selectize();

var stateSelectize = $('.state-selectize');
var citySelectize = $('.city-selectize');
$('.country-selectize').selectize({
   onChange(value){
       if (!value.length) return;
       stateSelectize[0].selectize.clearOptions();
       stateSelectize[0].selectize.load((callback) => {
           axios.get('/api/countries/' + value)
               .then((result) => {
                   callback(result.data.states);
                   stateSelectize[0].selectize.enable();
               }).catch();
       })
   }
});

stateSelectize.selectize({
    valueField: 'id',
    labelField: 'name',
    onChange(value){
        if (!value.length) return;
        citySelectize[0].selectize.clearOptions();
        citySelectize[0].selectize.load((callback) => {
            axios.get('/api/states/' + value)
                .then((result) => {
                    callback(result.data.cities);
                    citySelectize[0].selectize.enable();
                }).catch();
        })
    }
});
citySelectize.selectize({
    valueField: 'id',
    labelField: 'name'
});
citySelectize[0].selectize.disable();
stateSelectize[0].selectize.disable();