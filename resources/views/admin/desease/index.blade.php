@extends('admin.layout.main')

@section('title', 'Заболевания')

@section('actions')
    <div class="links line-height-1">
        @can('desease-create')
            <a href="{{ route('roles.create') }}">Создать</a>
        @endcan
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список ролей</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped card-table ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Источник</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($deseases as $desease)
                                <tr>
                                    <td class="w-1">{{ $desease->id }}</td>
                                    <td><a href="{{ route('deseases.show', ['id' => $desease->id]) }}">{{ $desease->name }}</a></td>
                                    <td>{{ $desease->description }}</td>
                                    <td>{{ $desease->source }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $deseases->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection