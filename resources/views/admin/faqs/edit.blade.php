@extends('admin.layout.main')

@section('title', 'Создание статьи')

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('faqs.update', ['id' => $faq->id]) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input type="text" id="title" name="title" placeholder="Как сделать..." class="form-control" value="{{ $faq->title }}">
                        </div>

                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea name="description" id="description" rows="10" placeholder="Для выполнения определенных действий необходимо" class="form-control">{{ $faq->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="tags">Теги</label>
                            <select name="tags[]" id="tags" multiple class="form-control js-selectize"
                                    placeholder="Теги">
                                <option value=""></option>
                                @foreach($tags as $tag)
                                    <option value="{{ $tag->id }}" {{ $faq->tags->where('id', $tag->id)->count() > 0 ? 'selected' : '' }}>{{ $tag->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection