@extends('admin.layout.main')

@section('title', 'Faqs')

@section('actions')
    <div class="links line-height-1">
        <a href="{{ route('faqs.create') }}">Создать</a>
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список статей</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Теги</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($faqs as $faq)
                                <tr>
                                    <td class="w-1">{{ $faq->id }}</td>
                                    <td><a href="{{ route('faqs.edit', ['id' => $faq->id]) }}">{{ $faq->title }}</a></td>
                                    <td class="tags">
                                        @foreach($faq->tags as $tag)
                                            <span class="tag tag-primary">{{ $tag->name }}</span>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $faqs->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection