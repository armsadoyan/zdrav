@extends('admin.layout.main')

@section('title', 'Создание типа анкеты')

@section('actions')
    <div class="links line-height-1">
        <a href="{{ route('forms.create') }}">Создать тип анкеты</a>
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('forms.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="services">Название</label>
                                <input type="text" class="form-control" name="name" id="services" placeholder="Название">
                            </div>

                            <div class="col-12 form-group">
                                <label for="image">Изображение</label>
                                <input type="file" class="form-control-file" name="image" id="image" placeholder="Изображение">
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary">Создать</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection