@extends('admin.layout.main')

@section('title', 'Анкеты')

@section('actions')
    <div class="links line-height-1">
        @can('object-create')
        <a href="{{ route('forms.create') }}">Создать</a>
        @endcan
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список анкет</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($forms as $form)
                                <tr>
                                    <td class="w-1">{{ $form->id }}</td>
                                    <td>{{ $form->name }}</td>
                                    <td>{{ $form->created_at->format('m/d/Y') }}</td>
                                    <td>{{ $form->updated_at->format('m/d/Y') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection