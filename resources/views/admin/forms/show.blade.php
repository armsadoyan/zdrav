@extends('admin.layout.main')

@section('title', 'Просмотр Анкеты')

@section('content')
    @include('admin.parts.messages')

    <div class="row">
        <div class="col-lg-7">
            <div class="card position-relative">
                <div>
                    <img src="{{ 'http://18.191.209.21/profile/anketas/'.$cards->image_url }}" alt="" class="card-img-top">
                </div>
                <div class="card-body">
                    <h1>{{ $cards->name }}</h1>
                </div>
                @canany(['cards-update', 'cards-delete'])
                <div class="card-actions">
                    @can('cards-update')
                    <a href="{{ route('cards.edit', ['id' => $cards->id]) }}"><i class="fa fa-pen"></i></a>
                    @endcan
                    @can('news-delete')
                    <form action="{{ route('cards.destroy', ['id' => $cards->id]) }}" method="POST" class="d-inline ml-2">
                        @csrf
                        @method('delete')
                        <button class="fa fa-times btn-unstyled"></button>
                    </form>
                    @endcan
                </div>
                @endcanany
            </div>
        </div>
    </div>
@endsection