@extends('admin.layout.main')

@section('title', 'Объекты')

@section('actions')
    <div class="links line-height-1">
        {{--@can('gallery-create')--}}
            <a href="{{ route('galleries.create') }}">Создать</a>
        {{--@endcan--}}
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Фото</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($galleries as $gallery)
                                <tr>
                                    <td class="w-1">{{ $gallery->id }}</td>
                                    <td><img class="gallery-img" src="{{ "http://18.191.209.21/profiles/gallery/". $gallery->img_name }}" /></td>
                                    <td>{{ $gallery->created_at->format('m/d/Y') }}</td>
                                    <td>{{ $gallery->updated_at->format('m/d/Y') }}</td>
                                    <td class="form-inline">
                                        @if($gallery->status == 'in_moderation')
                                        <form action="{{ route('galleries.update', ['id' => $gallery->id]) }}" method="POST" class="d-inline-block">
                                            @method('PUT')
                                            @csrf
                                            <button class="btn btn-sm btn-success d-inline-block mr-1">Принимать</button>
                                        </form>
                                        <form action="{{ route('galleries.destroy', ['id' => $gallery->id]) }}" method="POST" class="d-inline-block">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                        @elseif($gallery->status == 'accept')
                                            <p style="color:green">Принят</p>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $galleries->links() }}
                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        @csrf
                        <input type="text" name='pag_limit' class="form-control-sm" value="{{$limit}}" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection