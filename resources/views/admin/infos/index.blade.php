@extends('admin.layout.main')

@section('title', 'Объекты')

@section('actions')
    <div class="links line-height-1">
        {{--@can('object-create')--}}
            <a href="{{ route('informations.create') }}">Создать</a>
        {{--@endcan--}}
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Описание организации</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Звезды</th>
                                {{--<th>Активен?</th>--}}
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($infos as $info)
                                <tr>
                                    <td class="w-1">{{ $info->id }}</td>
                                    <td><a href="{{ route('informations.edit', ['id' => $info->id]) }}">{!! $info->organisation_description !!}</a></td>
                                    <td>{{ $info->created_at->format('m/d/Y') }}</td>
                                    <td>{{ $info->updated_at->format('m/d/Y') }}</td>
                                    <td><span class="tag tag-green">{{ $info->stars }}</span></td>
                                    <td class="form-inline">
                                        <a href="{{ route('informations.edit', ['id' => $info->id]) }}" class="btn btn-sm btn-primary d-inline-block mr-1">Изменить</a>
                                        <form action="{{ route('informations.destroy', ['id' => $info->id]) }}" method="POST" class="d-inline-block">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $infos->links() }}
                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        @csrf
                        <input type="text" name='pag_limit' class="form-control-sm" value="{{$limit}}" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection