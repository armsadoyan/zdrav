@extends('admin.layout.main')

@section('title', 'Просмотр информации')

@section('content')
    @include('admin.parts.messages')

    <div class="row">
        <div class="col-lg-7">
            <div class="card position-relative">
                <div class="card-body">
                    <div class="text-muted">
                        Описание организации:
                        {!! $info->organisation_description !!}
                    </div>

                    <div class="text-muted">
                        Условия оплаты:
                        {!! $info->payments_terms !!}
                    </div>

                    <div class="text-muted">
                        Документация:
                        {!! $info->docs !!}
                    </div>

                    <div class="text-muted">
                        Противопоказания:
                        {!! $info->contraindications !!}
                    </div>

                    <div class="text-muted">
                        Звезды: {{ $info->stars }}
                    </div>

                    <div class="text-muted">
                        Медицинские профили:
                        {{ $info->payments_terms }}
                    </div>

                    <div class="text-muted">
                        Методы лечения:
                        {{ $info->treatment_methods }}
                    </div>

                    <div class="text-muted">
                        Сервисы:
                        {{ $info->services }}
                    </div>

                    <div class="text-muted">
                        Создан:
                        {{ $info->created_at }}
                    </div>

                    <div class="text-muted">
                        Обновлен:
                        {{ $info->updated_at }}
                    </div>
                </div>
                @canany(['info-update', 'info-delete'])
                    <div class="card-actions">
                        @can('info-update')
                            <a href="{{ route('info.edit', ['id' => $info->id]) }}"><i class="fa fa-pen"></i></a>
                        @endcan
                        @can('info-delete')
                            <form action="{{ route('info.destroy', ['id' => $info->id]) }}" method="POST" class="d-inline ml-2">
                                @csrf
                                @method('delete')
                                <button class="fa fa-times btn-unstyled"></button>
                            </form>
                        @endcan
                    </div>
                @endcanany
            </div>
        </div>
    </div>
@endsection