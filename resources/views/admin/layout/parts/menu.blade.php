<li class="nav-item">
    <a href="{{ route('admin.index') }}" class="nav-link {{ request()->is('/') ? 'active' : '' }}"><i class="fe fe-home"></i> Главная</a>
</li>
@can('news-read')
<li class="nav-item">
    <a href="{{ route('news.index') }}" class="nav-link {{ request()->is('news') ? 'active' : '' }}"><i class="fe fe-file"></i> Новости</a>
</li>
@endcan
@can('role-read')
<li class="nav-item">
    <a href="{{ route('roles.index') }}" class="nav-link {{ request()->is('roles') ? 'active' : '' }}"><i class="fe fe-users"></i> Роли</a>
</li>
@endcan
@can('user-read')
<li class="nav-item">
    <a href="{{ route('users.index') }}" class="nav-link {{ request()->is('users') ? 'active' : '' }}"><i class="fe fe-user"></i> Пользователи</a>
</li>
@endcan
<li class="nav-item">
    <a href="{{ route('objects.index') }}" class="nav-link {{ request()->is('objects') ? 'active' : '' }}"><i class="fe fe-user"></i> Объекты</a>
</li>
<li class="nav-item">
    <a href="{{ route('faqs.index') }}" class="nav-link {{ request()->is('faqs') ? 'active' : '' }}"><i class="fe fe-list"></i> Faqs</a>
</li>
<li class="nav-item">
    <a href="{{ route('tags.index') }}" class="nav-link {{ request()->is('tags') ? 'active' : '' }}"><i class="fe fe-hash"></i> Теги</a>
</li>
<li class="nav-item">
    <a href="{{ route('locations.index') }}" class="nav-link {{ request()->is('locations') ? 'active' : '' }}"><i class="fe fe-user"></i> Локации</a>
</li>
<li class="nav-item">
    <a href="{{ route('informations.index') }}" class="nav-link {{ request()->is('informations') ? 'active' : '' }}"><i class="fe fe-user"></i> Информации</a>
</li>
<li class="nav-item">
    <a href="{{ route('forms.index') }}" class="nav-link {{ request()->is('forms') ? 'active' : '' }}"><i class="fe fe-list"></i> Анкеты</a>
</li>
<li class="nav-item">
    <a href="{{ route('galleries.index') }}" class="nav-link {{ request()->is('galleries') ? 'active' : '' }}"><i class="fe fe-list"></i> Галерея</a>
</li>