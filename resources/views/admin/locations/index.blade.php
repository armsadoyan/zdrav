@extends('admin.layout.main')

@section('title', 'Локации')

@section('content')
    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список стран</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($countries as $country)
                                <tr>
                                    <td class="w-1">{{ $country->id }}</td>
                                    <td>{{ $country->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $countries->links() }}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список регионов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Страна</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($states as $state)
                                <tr>
                                    <td class="w-1">{{ $state->id }}</td>
                                    <td>{{ $state->name }}</td>
                                    <td>{{ $state->country->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $states->links() }}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список городов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Регион</th>
                                <th>Страна</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cities as $city)
                                <tr>
                                    <td class="w-1">{{ $city->id }}</td>
                                    <td>{{ $city->name }}</td>
                                    <td>{{ $city->state->name }}</td>
                                    <td>{{ $city->state->country->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $countries->links() }}
                </div>
            </div>
        </div>
        {{--@can('tag-create')--}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Создание страны</div>
                </div>
                <div class="card-body">
                    <form action="{{ route('locations.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="country">
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" name="name" id="name" placeholder="отдых" class="form-control">
                        </div>
                        <button class="btn btn-primary">Создать</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Создание региона</div>
                </div>
                <div class="card-body">
                    <form action="{{ route('locations.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="state">
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" name="name" id="name" placeholder="отдых" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="country_id">Страна</label>
                            <select name="country_id" id="country_id" class="form-control js-selectize">
                                @foreach(App\Models\Country::all() as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary">Создать</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Создание населенного пункта</div>
                </div>
                <div class="card-body">
                    <form action="{{ route('locations.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="city">
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" name="name" id="name" placeholder="отдых" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="state_id">Регион</label>
                            <select name="state_id" id="state_id" class="form-control js-selectize">
                                @foreach(App\Models\State::all() as $state)
                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary">Создать</button>
                    </form>
                </div>
            </div>
        </div>
        {{--@endcan--}}
    </div>
@endsection