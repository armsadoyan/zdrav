@extends('admin.layout.main')

@section('title', 'Редактирование новости')

@section('content')

    @include('admin.parts.messages')

    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('news.update', ['id' => $news->id]) }}" method="POST">
                        @method('put')
                        @csrf
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Название материала"
                                value="{{ $news->title }}">
                            </div>

                            <div class="col-12 form-group">
                                <label for="short_text">Краткое содержание</label>
                                <textarea type="text" class="form-control" name="short_text" id="title" placeholder="Краткое содержание материала. Урезается до 150 символов" rows="6">{{ $news->short_text }}</textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="full_text">Полное содержание</label>
                                <textarea type="text" class="form-control" name="full_text" id="full_text" placeholder="Полное содержание новости" rows="12">{{ $news->full_text }}</textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="meta_keywords">Метатег keywords</label>
                                <input value="{{ $news->meta_keywords }}" type="text" class="form-control" name="meta_keywords" id="meta_keywords" placeholder="Ключевые слова">
                            </div>

                            <div class="col-12 form-group">
                                <label for="meta_description">Метатег description</label>
                                <input value="{{ $news->meta_description }}" type="text" class="form-control" name="meta_description" id="meta_description" placeholder="Описание">
                            </div>

                            <div class="col-12 form-group">
                                <label for="is_visible" class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="is_visible" id="is_visible"
                                    {{ $news->is_visible ? 'checked' : '' }}>
                                    <span class="custom-control-label">Новость видима</span>
                                </label>

                                <label for="is_published" class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="is_published" id="is_published"
                                    {{ $news->is_published ? 'checked' : '' }}>
                                    <span class="custom-control-label">Новость опубликована на главной</span>
                                </label>
                            </div>

                            <div class="col-12 form-group">
                                <label for="date">Дата публикации записи</label>
                                <input type="text" placeholder="Дата в формате YYYY-mm-dd H:i:s" class="form-control" name="date" value="{{ \Carbon\Carbon::parse($news->date)->format('Y-m-d H:i:s') }}">
                                <small class="small">год-месяц-день часы-минуты-секунды</small>
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary">Обновить новость</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection