@extends('admin.layout.main')

@section('title', 'Новости')

@section('actions')
    <div class="links line-height-1">
        @can('news-create')
            <a href="{{ route('news.create') }}">Создать</a>
        @endcan
    </div>
@endsection

@section('content')
    @include('admin.parts.messages')

    <div class="row row-cards row-desk">
        @foreach($news as $item)
            <div class="col-lg-3">
                <div class="card position-relative">
                    <a href="{{ route('news.edit', ['news' => $item->id]) }}">
                        <img src="{{ 'http://18.191.209.21/news/'.$item->picture }}" alt="" class="card-img-top">
                    </a>
                    <div class="card-body d-flex flex-column">
                        <h4><a href="{{ route('news.show', ['id' => $item->id]) }}">{{ $item->title }}</a></h4>
                        <div class="text-muted">
                            {!! str_limit($item->short_text, 100) !!}
                        </div>
                    </div>
                    @canany(['news-update'])
                        <div class="card-actions">
                            @can('news-update')
                                <a href="{{ route('news.edit', ['id' => $item->id]) }}"><i class="fa fa-pen"></i></a>
                            @endcan
                            {{--<a href="#"><i class="fa fa-times"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-eye"></i></a>--}}
                        </div>
                    @endcanany
                </div>
            </div>
        @endforeach
    </div>
    {{ $news->links() }}
@endsection