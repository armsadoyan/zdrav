@extends('admin.layout.main')

@section('title', 'Просмотр новости')

@section('content')
    @include('admin.parts.messages')

    <div class="row">
        <div class="col-lg-7">
            <div class="card position-relative">
                <div>
                    <img src="{{ 'http://18.191.209.21/news/'.$news->picture }}" alt="" class="card-img-top">
                </div>
                <div class="card-body">
                    <h1>{{ $news->title }}</h1>
                    <div class="text-muted">
                        @markdown($news->full_text)
                    </div>
                </div>
                @canany(['news-update', 'news-delete'])
                <div class="card-actions">
                    @can('news-update')
                    <a href="{{ route('news.edit', ['id' => $news->id]) }}"><i class="fa fa-pen"></i></a>
                    @endcan
                    @can('news-delete')
                    <form action="{{ route('news.destroy', ['id' => $news->id]) }}" method="POST" class="d-inline ml-2">
                        @csrf
                        @method('delete')
                        <button class="fa fa-times btn-unstyled"></button>
                    </form>
                    @endcan
                </div>
                @endcanany
            </div>
        </div>
    </div>
@endsection