@extends('admin.layout.main')

@section('title', 'Создание объекта')

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('objects.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input type="text" id="title" name="name" placeholder="Санаторий им. Фрунзе" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="url">Адрес в поисковой строке (латиница)</label>
                            <input type="text" id="url" name="url" placeholder="qwe123" class="form-control">
                            <small class="text-muted">Важно! Адрес, в случае указания этого поля будет следующим: http://zdravproduct.com/objects/{ваш урл}</small>
                        </div>

                        <div class="form-group">
                            <label for="region">Регион</label>
                            <div class="d-flex" id="region">
                                <div class="col-md-4 ml-0 pl-0">
                                    <select name="country_id" id="country" class="form-control country-selectize" placeholder="Страна">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 ml-0 pl-0">
                                    <select name="state_id" id="state" class="state-selectize form-control" placeholder="Область / Регион">
                                        <option value=""></option>
                                        {{--@foreach($states as $state)--}}
                                            {{--<option value="{{ $state->id }}">{{ $state->name }}</option>--}}
                                        {{--@endforeach--}}
                                    </select>
                                </div>
                                <div class="col-md-4 ml-0 pl-0">
                                    <select name="city_id" id="city" class="city-selectize form-control" placeholder="Город">
                                        <option value=""></option>
                                        {{--@foreach($states as $state)--}}
                                        {{--<option value="{{ $state->id }}">{{ $state->name }}</option>--}}
                                        {{--@endforeach--}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea name="description" id="description" rows="10" placeholder="Прекрасное место..." class="form-control"></textarea>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="Награды">Регион</label>--}}
                            {{--<select name="country" id="country" class="form-control country-selectize" placeholder="Страна">--}}
                                {{--<option value=""></option>--}}
                                {{--@foreach($countries as $country)--}}
                                    {{--<option value="{{ $country->id }}">{{ $country->name }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label for="stars">Звездность</label>
                            <div class="d-flex flex-wrap" id="stars">
                                <div class="custom-control custom-radio mr-2">
                                    <input id="star1" name="stars" value="1" type="radio" class="custom-control-input">
                                    <label for="star1" class="custom-control-label">1</label>
                                </div>
                                <div class="custom-control custom-radio mr-2">
                                    <input id="star2" name="stars" value="2" type="radio" class="custom-control-input">
                                    <label for="star2" class="custom-control-label">2</label>
                                </div>
                                <div class="custom-control custom-radio mr-2">
                                    <input id="star3" name="stars" value="3" type="radio" class="custom-control-input">
                                    <label for="star3" class="custom-control-label">3</label>
                                </div>
                                <div class="custom-control custom-radio mr-2">
                                    <input id="star4" name="stars" value="4" type="radio" class="custom-control-input">
                                    <label for="star4" class="custom-control-label">4</label>
                                </div>
                                <div class="custom-control custom-radio mr-2">
                                    <input id="star5" name="stars" value="5" type="radio" class="custom-control-input">
                                    <label for="star5" class="custom-control-label">5</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="payment_conditions">Условия оплаты</label>
                            <textarea name="payment_conditions" id="payment_conditions" rows="10" placeholder="Оплата принимается ..." class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="required_documents">Необходимые документы</label>
                            <textarea name="required_documents" id="required_documents" rows="10" placeholder="Паспорт..." class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="visa_information">Информация о визе</label>
                            <textarea name="visa_information" id="visa_information" rows="10" placeholder="Виза оформляется за..." class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="contraindications">Противопоказания</label>
                            <textarea name="contraindications" id="contraindications" rows="10" placeholder="Запрещено для людей, страдающих.." class="form-control"></textarea>
                        </div>

                        <div class="form-group position-relative">
                            <label for="medical_profiles">Медицинские профили</label>
                            <select name="medical_profiles[]" id="medical_profiles" class="form-control custom-select js-selectize h-auto" multiple>
                                <option value=""></option>
                                @foreach($medicalProfiles as $medicalProfile)
                                    <option value="{{ $medicalProfile->id }}">{{ $medicalProfile->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="therapies">Методы лечения</label>
                            <select name="therapies[]" id="therapies" placeholder="" multiple class="form-control js-selectize h-auto">
                                <option value=""></option>
                                @foreach($therapies as $therapy)
                                    <option value="{{$therapy->id}}">{{$therapy->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="custom-switch" for="is_published" >
                                <input type="checkbox" id="is_published" name="is_published" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Опубликовано</span>
                            </label>

                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection