@extends('admin.layout.main')

@section('title', 'Объекты')

@section('actions')
    <div class="links line-height-1">
        @can('object-create')
            <a href="{{ route('objects.create') }}">Создать</a>
        @endcan
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                    <div class="search">
                        <input type="text" id="search" />
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Номера</th>
                                <th>Активен?</th>
                                {{--<th>Действия</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($objects as $object)
                                <tr>
                                    <td class="w-1">{{ $object->id }}</td>
                                    {{--<td><a href="{{ route('objects.edit', ['id' => $object->id]) }}">{{ $object->name }}</a></td>--}}
                                    <td>{{ $object->name }}</td>
                                    <td>{{ $object->created_at->format('m/d/Y') }}</td>
                                    <td>{{ $object->updated_at->format('m/d/Y') }}</td>
                                    <td><span class="tag tag-green">Номеров: {{ $object->rooms->count() }}</span></td>
                                    <td><span class="tag tag-{{ $object->is_published ? 'green' : 'red' }}">{{ $object->is_active ? 'Да' : 'Нет' }}</span></td>
                                    {{--<td class="form-inline">--}}
                                        {{--<a href="{{ route('objects.edit', ['id' => $object->id]) }}" class="btn btn-sm btn-primary d-inline-block mr-1">Изменить</a>--}}
                                        {{--<form action="{{ route('objects.destroy', ['id' => $object->id]) }}" method="POST" class="d-inline-block">--}}
                                            {{--@method('DELETE')--}}
                                            {{--@csrf--}}
                                            {{--<button class="btn btn-sm btn-outline-danger">Удалить</button>--}}
                                        {{--</form>--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $objects->appends(\Request::get('query'))->links() }}
                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        @csrf
                        <input type="text" name='pag_limit' class="form-control-sm" value="{{$limit}}" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){

            // fetch_customer_data();

            function fetch_customer_data(query = '')
            {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                $.ajax({
                    url:"/search?page=" + vars['page'],
                    method:'GET',
                    data:{query:query},
                    dataType:'json',
                    success:function(data)
                    {
                        $('tbody').html(data.table_data);
                        $('#total_records').text(data.total_data);
                    }
                })
            }

            $('#search').on('keyup', function(){
                var query = $(this).val();
                fetch_customer_data(query);
            });
        });
    </script>
@endsection