@extends('admin.layout.main')

@section('title', 'Роли')

@section('content')
    @include('admin.parts.messages')

    <div class="row row-cards row-desk">
        <div class="col-md-9">
            <form method="POST" action="{{ route('roles.update', ['id' => $role->id]) }}" class="card">
                @csrf
                @method('PUT')
                <div class="card-header">
                    <div class="card-title">Редактирование роли {{ $role->name }}</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Название роли</label>
                        <input type="text" maxlength="25" name="name" id="name" class="form-control" placeholder="Название роли" value="{{ $role->name }}">
                        <small class="text-muted">Будет отображаться в меню</small>
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" maxlength="25" name="slug" id="slug" class="form-control" placeholder="slug" value="{{ $role->slug }}">
                        <small class="text-muted">Короткое название на латинском языке, необходимое для работы приложения. Без пробелов - использовать символ нижнего подчеркивания. Обязано быть уникальным. Пример admin, user, writing_user. </small>
                    </div>
                    <div class="form-group">
                        <label class="mb-4">Разрешения</label>
                        @foreach($permissions as $permissionKey=>$permissionList)
                            <div class="permissions">
                                <label for="{{ $permissionKey }}" class="mb-1">{{ $permissionKey }}</label>
                                <div class="d-flex flex-wrap mb-3">
                                    @foreach($permissionList as $permission)
                                        <label for="perm{{ $permission->id }}" class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="perm{{ $permission->id }}"
                                                   {{ $permission->has_default ? 'readonly disabled' : '' }}
                                                   {{ $role->permissions->where('id', $permission->id)->count() > 0 ? 'checked' : ''}}
                                                   @if( !$permission->has_default)
                                                   name="permission[{{ $permission->id }}]"
                                                    @endif
                                            >
                                            <span class="custom-control-label">{{ $permission->name }}</span>

                                            @if($permission->has_default)
                                                <input type="hidden" name="permission[{{ $permission->id }}]" value="on">
                                            @endif
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary">Обновить</button>
                </div>
            </form>
        </div>
    </div>
@endsection