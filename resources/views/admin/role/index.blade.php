@extends('admin.layout.main')

@section('title', 'Роли')

@section('actions')
    <div class="links line-height-1">
        @can('role-create')
            <a href="{{ route('roles.create') }}">Создать</a>
        @endcan
    </div>
@endsection

@section('content')

    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список ролей</div>
                </div>
                <div class="card-body">
                    <div class="list-group">
                        @foreach($roles as $role)
                            <a class="list-group-item link-normal" href="{{ route('roles.show', ['id' => $role->id]) }}">
                                <div class="d-flex flex-wrap">
                                    <h5 class="mb-1 mr-3">{{ $role->name }}</h5>
                                    <small class="small text-muted">{{ $role->slug }}</small>
                                </div>
                                <div class="mt-1 text-muted">
                                    {{ $role->permissions->implode('name', ', ') }}
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection