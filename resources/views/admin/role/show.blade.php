@extends('admin.layout.main')

@section('title', 'Роли')

@section('content')
    @include('admin.parts.messages')

    <div class="row row-cards row-desk">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="card-title mr-3">
                        {{ $role->name }}
                    </div>
                    <small class="text-muted line-height-1">{{ $role->slug }}</small>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="mb-4">Разрешения</label>
                        @foreach($permissions as $permissionKey=>$permissionList)
                            <div class="permissions">
                                <label for="{{ $permissionKey }}" class="mb-1">{{ $permissionKey }}</label>
                                <div class="d-flex flex-wrap mb-3">
                                    @foreach($permissionList as $permission)
                                        <label for="perm{{ $permission->id }}"
                                               class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="perm{{ $permission->id }}"
                                                   disabled
                                                    {{ $permission->has_default || $role->permissions->where('id', $permission->id)->count()>0 ? 'checked' : '' }}
                                            >
                                            <span class="custom-control-label">{{ $permission->name }}</span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @if($role->is_editable)
                    <div class="card-footer">
                        @can('role-update')

                            <a href="{{ route('roles.edit', ['id' => $role->id]) }}" class="btn btn-primary">Редактировать</a>

                        @endcan
                        @can('role-delete')
                            <form action="{{ route('roles.destroy', ['id' => $role->id]) }}" method="POST"
                                  class="d-inline-block">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger">Удалить</button>
                            </form>
                        @endcan
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection