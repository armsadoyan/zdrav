@extends('admin.layout.main')

@section('title', 'Теги')

@section('content')
    @include('admin.parts.messages')
    <div class="row row-cards row-desk">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список тегов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Slug</th>
                                <th>Кол-во записей</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tags as $tag)
                                <tr>
                                    <td class="w-1">{{ $tag->id }}</td>
                                    <td>{{ $tag->name }}</td>
                                    <td>{{ $tag->slug }}</td>
                                    <td>{{ $tag->faqs()->count() }}</td>
                                    <td>
                                        <form action="{{ route('tags.destroy', ['id' => $tag->id]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $tags->links() }}
                </div>
            </div>
        </div>
        {{--@can('tag-create')--}}
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Создание тега</div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('tags.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="name">Имя</label>
                                <input type="text" name="name" id="name" placeholder="отдых" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug">Уникальный идентификатор</label>
                                <input type="text" name="slug" id="slug" placeholder="rest" class="form-control">
                            </div>
                            <button class="btn btn-primary">Создать</button>
                        </form>
                    </div>
                </div>
            </div>
        {{--@endcan--}}
    </div>
@endsection