@extends('admin.layout.main')

@section('title', 'Пользователи')

@section('content')
    @include('admin.parts.messages')

    <div class="row row-cards row-desk">
        <div class="col-md-6">
            <form method="POST" action="{{ route('users.update', ['id' => $user->id]) }}" class="card">
                @csrf
                @method('PUT')
                <div class="card-header">
                    <div class="card-title">Редактирование пользователя {{ $user->name }}</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="email">Электронная почта</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ $user->email }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Имя пользователя</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Имя пользователя" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Фамилия пользователя</label>
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Фамилия пользователя" value="{{ $user->last_name }}">
                    </div>

                    <div class="form-group">
                        <label for="father_name">Отчество пользователя</label>
                        <input type="text" name="father_name" id="father_name" class="form-control" placeholder="Отчество пользователя" value="{{ $user->father_name }}">
                    </div>

                    <div class="form-group">
                        <label for="role_id">Роль</label>
                        <select name="role_id" id="role_id" class="custom-select form-control">
                            <option value="">Без роли</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}"
                                    {{ $user->role_id == $role->id ? 'selected' : '' }}
                                >{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary">Обновить</button>
                </div>
            </form>
        </div>
    </div>
@endsection