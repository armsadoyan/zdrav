@extends('admin.layout.main')

@section('title', 'Пользователи')

@section('content')
    @include('admin.parts.messages')

    <div class="row">
        <div class="col-12">
            <div class="card position-relative">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table card-table table-striped table-vcenter">
                            <thead>
                                <tr>
                                    <th colspan="2">Пользователь</th>
                                    <th>Email</th>
                                    <th>Дата регистрации</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td class="w-1">
                                    <span class="avatar avatar-white" style="background-image: url('{{ $user->image }}')"></span>
                                </td>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->created_at }}
                                </td>
                                <td class="w-1">
                                    @can('user-update')
                                        <a href="{{ route('users.edit', ['id' => $user->id]) }}" class="icon mr-2"><i class="fa fa-pen"></i></a>
                                    @endcan
                                    @can('user-delete')
                                        <form action="{{ route('users.destroy', ['id' => $user->id]) }}" method="POST" class="d-inline">
                                            @csrf
                                            @method('DELETE')
                                            <button class="icon btn-unstyled"><i class="fa fa-trash"></i></button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection