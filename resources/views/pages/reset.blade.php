@extends('pages.layout')

@section('page-content')
    <div class="page-single">
        <div class="container">
            <div class="row">
                <div class="col col-login mx-auto">
                    <form class="card" action="{{ route('password.request') }}" method="post">
                        {{ csrf_field() }}
                        <div class="card-body p-6">
                            <div class="card-title">Создайте новый пароль</div>
                            <div class="form-group">
                                <label class="form-label">E-mail</label>
                                <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="mail@example.com" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <small class="invalid-feedback">
                                        {{$errors->first('email')}}
                                    </small>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="form-label">
                                    Пароль
                                </label>
                                <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="exampleInputPassword1" placeholder="Пароль">
                                @if ($errors->has('password'))
                                    <small class="invalid-feedback">
                                        {{$errors->first('password')}}
                                    </small>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-label">Запомнить меня</span>
                                </label>
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary btn-block">Войти</button>
                            </div>
                        </div>
                    </form>
                    <div class="text-muted text-center small">
                        Вспомнили пароль? <a href="{{ route('login') }}">Войдите, используя старый.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection