<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
use Illuminate\Support\Facades\Route;

Route::name('api.')->namespace('Api')->group(function () {
    Route::resource('objects', 'ObjectController')->only(['index', 'show']);
    Route::resource('news', 'NewsController')->only(['index', 'show']);
    Route::resource('deseases', 'DeseaseController')->only(['index']);
    Route::resource('medical_profiles', 'MedicalProfileController')->only(['index']);
    Route::resource('therapies', 'TherapyController')->only(['index']);
    Route::resource('countries', 'CountryController')->only(['index', 'show']);
    Route::resource('states', 'StateController')->only(['index', 'show']);

    Route::middleware('auth:api')->group(function () {
        Route::post('profile', 'UserController@update');
        Route::resource('form', 'FormController')->only(['index', 'store']);
        Route::post('gallery/add', 'ObjectController@addImages');
    });
});

Route::post('auth/login', 'Api\Auth\LoginController@login');
Route::get('auth/logout', 'Api\Auth\LogoutController@logout')->middleware(['auth:api']);
Route::get('me', 'Api\Auth\UserInfoController@info')->middleware(['auth:api']);

Route::post('addNews','Api\NewsController@addNews');
Route::get('getAllNews','Api\NewsController@getAllNews');
Route::get('getNewsById/{id}','Api\NewsController@getNewsById');
Route::put('editNews/{id}','Api\NewsController@edit');
Route::delete('news/destroy/{id}', 'Api\NewsController@destroy');

Route::post('objinfo','Api\InfoController@addInfo');
Route::get('objinfo/{id}','Api\InfoController@getInfo');
Route::delete('info/destroy/{id}', 'Api\InfoController@destroy');
Route::post('acceptOrDeleteModeration','Api\InfoController@acceptOrDeleteModeration');

//Route::post('addOrUpdateCard','Api\CardController@addOrUpdateCard');
Route::post('form','Api\CardController@form');
//Route::get('getCard','Api\CardController@getCard');
Route::get('form/{id}','Api\CardController@form');
Route::delete('card/destroy/{id}', 'Api\CardController@destroy');

//Route::post('addImageToGallery','Api\GalleryController@add');
Route::post('objgallery', 'Api\GalleryController@add');
Route::get('objgallery/{id}', 'Api\GalleryController@getMyGallery');
Route::get('getPhotosInModeration', 'Api\GalleryController@getPhotosInModeration');
Route::post('changePhotoStatus','Api\GalleryController@changePhotoStatus');
//Route::get('getMyGallery','Api\GalleryController@getMyGallery');

