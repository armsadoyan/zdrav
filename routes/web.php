<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->namespace('Dashboard')->group(function () {
    Route::get('/', 'IndexController@index')->name('admin.index');
    Route::resource('news', 'NewsController');
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController')->except(['show']);
    Route::resource('deseases', 'DeseaseController');
    Route::resource('objects', 'ObjectController')->only(['index', 'create', 'edit', 'store', 'update', 'destroy']);
    Route::post('pagination_limit', 'ObjectController@changePaginationLimit');
    Route::resource('faqs', 'FaqController')->only(['index', 'edit', 'create', 'store', 'update', 'destroy']);
    Route::resource('tags', 'TagController')->only(['index', 'store', 'update', 'destroy']);
    Route::get('search{page?}', 'ObjectController@search');
    Route::resource('locations', 'LocationController');
    Route::resource('informations', 'InfoController');
    Route::resource('forms', 'CardsController');
    Route::resource('galleries', 'GalleryController');
});