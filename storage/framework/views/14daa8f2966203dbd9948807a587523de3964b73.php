<?php $__env->startSection('title', 'Заболевания'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('desease-create')): ?>
            <a href="<?php echo e(route('roles.create')); ?>">Создать</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список ролей</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped card-table ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Источник</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $deseases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $desease): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="w-1"><?php echo e($desease->id); ?></td>
                                    <td><a href="<?php echo e(route('deseases.show', ['id' => $desease->id])); ?>"><?php echo e($desease->name); ?></a></td>
                                    <td><?php echo e($desease->description); ?></td>
                                    <td><?php echo e($desease->source); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($deseases->links()); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>