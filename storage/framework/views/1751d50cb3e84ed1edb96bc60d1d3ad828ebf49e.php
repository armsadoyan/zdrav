<?php $__env->startSection('page-content'); ?>
    <div class="page-single">
        <div class="container">
            <div class="row">
                <div class="col col-login mx-auto">
                    <form class="card" action="<?php echo e(action('Auth\ResetPasswordController@reset')); ?>" method="post">
                        <?php echo e(csrf_field()); ?>

                        <div class="card-body p-6">
                            <div class="card-title">Забыли пароль?</div>
                            <p class="text-muted">Введите ваш адрес электронной почты и мы вышлем вам новый.</p>
                            <div class="form-group">
                                <label class="form-label" for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control <?php echo e($errors->has('email') ? 'is-invalid' : ''); ?>" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" >
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary btn-block">Отправьте мне пароль</button>
                            </div>
                        </div>
                    </form>
                    <div class="text-center text-muted small">
                        Вспомнил, <a href="<?php echo e(route('login')); ?>">верните меня</a> к окну авторизации.
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('pages.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>