<?php $__env->startSection('title', 'Роли'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role-create')): ?>
            <a href="<?php echo e(route('roles.create')); ?>">Создать</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список ролей</div>
                </div>
                <div class="card-body">
                    <div class="list-group">
                        <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a class="list-group-item link-normal" href="<?php echo e(route('roles.show', ['id' => $role->id])); ?>">
                                <div class="d-flex flex-wrap">
                                    <h5 class="mb-1 mr-3"><?php echo e($role->name); ?></h5>
                                    <small class="small text-muted"><?php echo e($role->slug); ?></small>
                                </div>
                                <div class="mt-1 text-muted">
                                    <?php echo e($role->permissions->implode('name', ', ')); ?>

                                </div>
                            </a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>