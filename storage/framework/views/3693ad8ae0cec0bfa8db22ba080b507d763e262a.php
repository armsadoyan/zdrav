<?php $__env->startSection('title', 'Объекты'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('object-create')): ?>
            <a href="<?php echo e(route('objects.create')); ?>">Создать</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                    <div class="search">
                        <input type="text" id="search" />
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Номера</th>
                                <th>Активен?</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                        
                                        
                                            
                                            
                                            
                                        
                                    
                                
                            
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($objects->appends(\Request::get('query'))->links()); ?>

                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" name='pag_limit' class="form-control-sm" value="<?php echo e($limit); ?>" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){

            fetch_customer_data();

            function fetch_customer_data(query = '')
            {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                $.ajax({
                    url:"/search?page=" + vars['page'],
                    method:'GET',
                    data:{query:query},
                    dataType:'json',
                    success:function(data)
                    {
                        console.log(data);
                        $('tbody').html(data.table_data);
                        $('#total_records').text(data.total_data);
                    }
                })
            }

            $('#search').on('keyup', function(){
                var query = $(this).val();
                fetch_customer_data(query);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>