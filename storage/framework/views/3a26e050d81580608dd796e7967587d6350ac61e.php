<?php $__env->startSection('title', 'Пользователи'); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row">
        <div class="col-12">
            <div class="card position-relative">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table card-table table-striped table-vcenter">
                            <thead>
                                <tr>
                                    <th colspan="2">Пользователь</th>
                                    <th>Email</th>
                                    <th>Дата регистрации</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="w-1">
                                    <span class="avatar avatar-white" style="background-image: url('<?php echo e($user->image); ?>')"></span>
                                </td>
                                <td>
                                    <?php echo e($user->name); ?>

                                </td>
                                <td>
                                    <?php echo e($user->email); ?>

                                </td>
                                <td>
                                    <?php echo e($user->created_at); ?>

                                </td>
                                <td class="w-1">
                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user-update')): ?>
                                        <a href="<?php echo e(route('users.edit', ['id' => $user->id])); ?>" class="icon mr-2"><i class="fa fa-pen"></i></a>
                                    <?php endif; ?>
                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user-delete')): ?>
                                        <form action="<?php echo e(route('users.destroy', ['id' => $user->id])); ?>" method="POST" class="d-inline">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('DELETE'); ?>
                                            <button class="icon btn-unstyled"><i class="fa fa-trash"></i></button>
                                        </form>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($users->links()); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>