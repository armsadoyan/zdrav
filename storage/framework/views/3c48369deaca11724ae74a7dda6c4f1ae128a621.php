<div class="d-flex">
    <a class="header-brand" href="/">
        Zdravproduct<span class="text-primary">Admin</span>
    </a>
    <div class="d-flex order-lg-2 ml-auto">
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <div class="dropdown">
            <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                <span class="avatar avatar-white" style="background-image: url('<?php echo e($currentUser->image); ?>')"></span>
                <span class="ml-2 d-none d-lg-block">
                      <span class="text-default"><?php echo e($currentUser->name); ?></span>
                      <small class="text-muted d-block mt-1"><?php echo e($currentUser->role ? $currentUser->role->name : ''); ?></small>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                <a class="dropdown-item" href="#">
                    <i class="dropdown-icon fe fe-user"></i> Профиль
                </a>
                <div class="dropdown-divider"></div>
                <form action="<?php echo e(route('logout')); ?>" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <button class="dropdown-item" type="submit">
                        <i class="dropdown-icon fe fe-log-out"></i> Выйти
                    </button>
                </form>
            </div>
        </div>
    </div>
    <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
        <span class="header-toggler-icon"></span>
    </a>
</div>