<?php $__env->startSection('title', 'Теги'); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список тегов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Slug</th>
                                <th>Кол-во записей</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="w-1"><?php echo e($tag->id); ?></td>
                                    <td><?php echo e($tag->name); ?></td>
                                    <td><?php echo e($tag->slug); ?></td>
                                    <td><?php echo e($tag->faqs()->count()); ?></td>
                                    <td>
                                        <form action="<?php echo e(route('tags.destroy', ['id' => $tag->id])); ?>" method="POST">
                                            <?php echo method_field('DELETE'); ?>
                                            <?php echo csrf_field(); ?>
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($tags->links()); ?>

                </div>
            </div>
        </div>
        
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Создание тега</div>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo e(route('tags.store')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                            <div class="form-group">
                                <label for="name">Имя</label>
                                <input type="text" name="name" id="name" placeholder="отдых" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug">Уникальный идентификатор</label>
                                <input type="text" name="slug" id="slug" placeholder="rest" class="form-control">
                            </div>
                            <button class="btn btn-primary">Создать</button>
                        </form>
                    </div>
                </div>
            </div>
        
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>