<?php $__env->startSection('title', 'Объекты'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        
            <a href="<?php echo e(route('galleries.create')); ?>">Создать</a>
        
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Фото</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="w-1"><?php echo e($gallery->id); ?></td>
                                    <td><img class="gallery-img" src="<?php echo e(asset('client/static/profiles/gallery/'. $gallery->img_name)); ?>" /></td>
                                    <td><?php echo e($gallery->created_at->format('m/d/Y')); ?></td>
                                    <td><?php echo e($gallery->updated_at->format('m/d/Y')); ?></td>
                                    <td class="form-inline">
                                        <?php if($gallery->status == 'in_moderation'): ?>
                                        <form action="<?php echo e(route('galleries.update', ['id' => $gallery->id])); ?>" method="POST" class="d-inline-block">
                                            <?php echo method_field('PUT'); ?>
                                            <?php echo csrf_field(); ?>
                                            <button class="btn btn-sm btn-success d-inline-block mr-1">Принимать</button>
                                        </form>
                                        <form action="<?php echo e(route('galleries.destroy', ['id' => $gallery->id])); ?>" method="POST" class="d-inline-block">
                                            <?php echo method_field('DELETE'); ?>
                                            <?php echo csrf_field(); ?>
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                        <?php elseif($gallery->status == 'accept'): ?>
                                            <p style="color:green">Принят</p>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($galleries->links()); ?>

                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" name='pag_limit' class="form-control-sm" value="<?php echo e($limit); ?>" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>