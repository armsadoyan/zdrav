<li class="nav-item">
    <a href="<?php echo e(route('admin.index')); ?>" class="nav-link <?php echo e(request()->is('/') ? 'active' : ''); ?>"><i class="fe fe-home"></i> Главная</a>
</li>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news-read')): ?>
<li class="nav-item">
    <a href="<?php echo e(route('news.index')); ?>" class="nav-link <?php echo e(request()->is('news') ? 'active' : ''); ?>"><i class="fe fe-file"></i> Новости</a>
</li>
<?php endif; ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role-read')): ?>
<li class="nav-item">
    <a href="<?php echo e(route('roles.index')); ?>" class="nav-link <?php echo e(request()->is('roles') ? 'active' : ''); ?>"><i class="fe fe-users"></i> Роли</a>
</li>
<?php endif; ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user-read')): ?>
<li class="nav-item">
    <a href="<?php echo e(route('users.index')); ?>" class="nav-link <?php echo e(request()->is('users') ? 'active' : ''); ?>"><i class="fe fe-user"></i> Пользователи</a>
</li>
<?php endif; ?>
<li class="nav-item">
    <a href="<?php echo e(route('objects.index')); ?>" class="nav-link <?php echo e(request()->is('objects') ? 'active' : ''); ?>"><i class="fe fe-user"></i> Объекты</a>
</li>
<li class="nav-item">
    <a href="<?php echo e(route('faqs.index')); ?>" class="nav-link <?php echo e(request()->is('faqs') ? 'active' : ''); ?>"><i class="fe fe-list"></i> Faqs</a>
</li>
<li class="nav-item">
    <a href="<?php echo e(route('tags.index')); ?>" class="nav-link <?php echo e(request()->is('tags') ? 'active' : ''); ?>"><i class="fe fe-hash"></i> Теги</a>
</li>
<li class="nav-item">
    <a href="<?php echo e(route('locations.index')); ?>" class="nav-link <?php echo e(request()->is('locations') ? 'active' : ''); ?>"><i class="fe fe-user"></i> Локации</a>
</li>
<li class="nav-item">
    <a href="<?php echo e(route('informations.index')); ?>" class="nav-link <?php echo e(request()->is('informations') ? 'active' : ''); ?>"><i class="fe fe-user"></i> Информации</a>
</li>
<li class="nav-item">
    <a href="<?php echo e(route('forms.index')); ?>" class="nav-link <?php echo e(request()->is('forms') ? 'active' : ''); ?>"><i class="fe fe-list"></i> Анкеты</a>
</li>
<li class="nav-item">
    <a href="<?php echo e(route('galleries.index')); ?>" class="nav-link <?php echo e(request()->is('galleries') ? 'active' : ''); ?>"><i class="fe fe-list"></i> Галерея</a>
</li>