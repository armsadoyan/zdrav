<?php $__env->startSection('title', 'Объекты'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        
            <a href="<?php echo e(route('informations.create')); ?>">Создать</a>
        
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Описание организации</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Звезды</th>
                                
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $infos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="w-1"><?php echo e($info->id); ?></td>
                                    <td><a href="<?php echo e(route('informations.edit', ['id' => $info->id])); ?>"><?php echo $info->organisation_description; ?></a></td>
                                    <td><?php echo e($info->created_at->format('m/d/Y')); ?></td>
                                    <td><?php echo e($info->updated_at->format('m/d/Y')); ?></td>
                                    <td><span class="tag tag-green"><?php echo e($info->stars); ?></span></td>
                                    <td class="form-inline">
                                        <a href="<?php echo e(route('informations.edit', ['id' => $info->id])); ?>" class="btn btn-sm btn-primary d-inline-block mr-1">Изменить</a>
                                        <form action="<?php echo e(route('informations.destroy', ['id' => $info->id])); ?>" method="POST" class="d-inline-block">
                                            <?php echo method_field('DELETE'); ?>
                                            <?php echo csrf_field(); ?>
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($infos->links()); ?>

                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" name='pag_limit' class="form-control-sm" value="<?php echo e($limit); ?>" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>