<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
    <title>Вход</title>
</head>
<body class="">
<div class="page">
    <?php $__env->startSection('page-content'); ?>
        <?php echo $__env->yieldSection(); ?>
</div>
</body>
</html>