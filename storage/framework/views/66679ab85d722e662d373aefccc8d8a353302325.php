<?php $__env->startSection('title', 'Создание новости'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <a href="<?php echo e(route('informations.create')); ?>">Создать dummy post</a>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="<?php echo e(route('informations.store')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="organisation_description">Описание организации</label>
                                <textarea type="text" class="form-control" name="organisation_description" id="organisation_description" placeholder="Описание организации" rows="6"></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="stars">Звезды</label>
                                <input type="text" class="form-control" name="stars" id="stars" placeholder="Звезды">
                            </div>

                            <div class="col-12 form-group">
                                <label for="payments_terms">Условия оплаты</label>
                                <textarea type="text" class="form-control" name="payments_terms" id="payments_terms" placeholder="Условия оплаты" rows="6"></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="docs">Документация</label>
                                <textarea type="text" class="form-control" name="docs" id="docs" placeholder="Документация" rows="6"></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="contraindications">Противопоказания</label>
                                <textarea type="text" class="form-control" name="contraindications" id="contraindications" placeholder="Противопоказания" rows="6"></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="medical_profiles">Медицинские профили</label>
                                <input type="text" class="form-control" name="medical_profiles" id="medical_profiles" placeholder="Медицинские профили">
                            </div>

                            <div class="col-12 form-group">
                                <label for="treatment_methods">Методы лечения</label>
                                <input type="text" class="form-control" name="treatment_methods" id="treatment_methods" placeholder="Методы лечения">
                            </div>

                            <div class="col-12 form-group">
                                <label for="services">Сервисы</label>
                                <input type="text" class="form-control" name="services" id="services" placeholder="Сервисы">
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary">Создать информация</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>