<?php $__env->startSection('title', 'Просмотр новости'); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row">
        <div class="col-lg-7">
            <div class="card position-relative">
                <div>
                    <img src="<?php echo e($news->image); ?>" alt="" class="card-img-top">
                </div>
                <div class="card-body">
                    <h1><?php echo e($news->title); ?></h1>
                    <div class="text-muted">
                        <?php echo app('Indal\Markdown\Parser')->parse($news->full_text); ?>
                    </div>
                </div>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->any(['news-update', 'news-delete'])): ?>
                <div class="card-actions">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news-update')): ?>
                    <a href="<?php echo e(route('news.edit', ['id' => $news->id])); ?>"><i class="fa fa-pen"></i></a>
                    <?php endif; ?>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news-delete')): ?>
                    <form action="<?php echo e(route('news.destroy', ['id' => $news->id])); ?>" method="POST" class="d-inline ml-2">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('delete'); ?>
                        <button class="fa fa-times btn-unstyled"></button>
                    </form>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>