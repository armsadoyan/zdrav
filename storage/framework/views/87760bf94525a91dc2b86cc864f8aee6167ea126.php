<?php $__env->startSection('title', 'Редактирование информации: ' . $info->id); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="<?php echo e(route('informations.update', ['id' => $info->id])); ?>" method="POST">
                        <input name="_method" type="hidden" value="PUT">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="organisation_description">Описание организации</label>
                                <textarea type="text" class="form-control" name="organisation_description" id="organisation_description" placeholder="Описание организации" rows="6"><?php echo e($info->organisation_description); ?></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="stars">Звезды</label>
                                <input type="text" class="form-control" name="stars" id="stars" placeholder="Звезды" value="<?php echo e($info->stars); ?>">
                            </div>

                            <div class="col-12 form-group">
                                <label for="payments_terms">Условия оплаты</label>
                                <textarea type="text" class="form-control" name="payments_terms" id="payments_terms" placeholder="Условия оплаты" rows="6"><?php echo e($info->payments_terms); ?></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="docs">Документация</label>
                                <textarea type="text" class="form-control" name="docs" id="docs" placeholder="Документация" rows="6"><?php echo e($info->docs); ?></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="contraindications">Противопоказания</label>
                                <textarea type="text" class="form-control" name="contraindications" id="contraindications" placeholder="Противопоказания" rows="6"><?php echo e($info->contraindications); ?></textarea>
                                <span class="small">Внимание! Данная форма поддерживает markdown синтаксис. Используйте его.</span>
                            </div>

                            <div class="col-12 form-group">
                                <label for="medical_profiles">Медицинские профили</label>
                                <input type="text" class="form-control" name="medical_profiles" id="medical_profiles" placeholder="Медицинские профили" value="<?php echo e($info->medical_profiles); ?>">
                            </div>

                            <div class="col-12 form-group">
                                <label for="treatment_methods">Методы лечения</label>
                                <input type="text" class="form-control" name="treatment_methods" id="treatment_methods" placeholder="Методы лечения" value="<?php echo e($info->treatment_methods); ?>">
                            </div>

                            <div class="col-12 form-group">
                                <label for="services">Сервисы</label>
                                <input type="text" class="form-control" name="services" id="services" placeholder="Сервисы" value="<?php echo e($info->services); ?>">
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary">Редактировать информация</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>