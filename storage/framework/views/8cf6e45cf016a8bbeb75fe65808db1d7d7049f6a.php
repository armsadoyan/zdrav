<?php $__env->startSection('content'); ?>
    <div class="container text-center">
        <div class="display-1 text-muted mb-5"><i class="si si-exclamation"></i> 401</div>
        <h1 class="h2 mb-3">Ошибка прав доступа</h1>
        <p class="h4 text-muted font-weight-normal mb-7"><?php echo e($message); ?></p>
        <a class="btn btn-primary" href="javascript:history.back()">
            <i class="fe fe-arrow-left mr-2"></i>Назад
        </a>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.blank', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>