<?php if(session()->has('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session()->get('status')); ?>

    </div>
<?php endif; ?>

<?php if(session()->has('error')): ?>
    <div class="alert alert-danger">
        <?php echo e(session()->get('error')); ?>

    </div>
<?php endif; ?>

<?php if(session()->has('warning')): ?>
    <div class="alert alert-warning">
        <?php echo e(session()->get('warning')); ?>

    </div>
<?php endif; ?>

<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>