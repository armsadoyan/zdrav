<?php $__env->startSection('title', 'Создание объекта'); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="<?php echo e(route('galleries.store')); ?>" method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="form-group">
                            <label for="title">Фото</label>
                            <input name='img_name' type="file" id='image' class="form-control">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>