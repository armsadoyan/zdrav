<?php $__env->startSection('page-content'); ?>
    <div class="page-single">
        <div class="container">
            <div class="row">
                <div class="col col-login mx-auto">
                    <form class="card" action="<?php echo e(route('login')); ?>" method="post">
                        <?php echo e(csrf_field()); ?>

                        <div class="card-body p-6">
                            <div class="card-title">Войдите в свой аккаунт</div>
                            <div class="form-group">
                                <label class="form-label">E-mail</label>
                                <input type="email" name="email" class="form-control <?php echo e($errors->has('email') ? 'is-invalid' : ''); ?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="mail@example.com" value="<?php echo e(old('email')); ?>">
                                <?php if($errors->has('email')): ?>
                                    <small class="invalid-feedback">
                                        <?php echo e($errors->first('email')); ?>

                                    </small>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label class="form-label">
                                    Пароль
                                    <a href="<?php echo e(route('password.request')); ?>" class="float-right small">Я забыл пароль</a>
                                </label>
                                <input type="password" name="password" class="form-control <?php echo e($errors->has('password') ? 'is-invalid' : ''); ?>" id="exampleInputPassword1" placeholder="Пароль">
                                <?php if($errors->has('password')): ?>
                                    <small class="invalid-feedback">
                                        <?php echo e($errors->first('password')); ?>

                                    </small>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" />
                                    <span class="custom-control-label">Запомнить меня</span>
                                </label>
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary btn-block">Войти</button>
                            </div>
                        </div>
                    </form>
                    <div class="text-muted text-center small">
                        Нет аккаунта администратора? <a href="<?php echo e(route('register')); ?>">Запросите</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('pages.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>