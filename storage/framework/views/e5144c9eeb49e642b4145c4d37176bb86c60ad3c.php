<?php $__env->startSection('title', 'Просмотр информации'); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row">
        <div class="col-lg-7">
            <div class="card position-relative">
                <div class="card-body">
                    <div class="text-muted">
                        Описание организации:
                        <?php echo $info->organisation_description; ?>

                    </div>

                    <div class="text-muted">
                        Условия оплаты:
                        <?php echo $info->payments_terms; ?>

                    </div>

                    <div class="text-muted">
                        Документация:
                        <?php echo $info->docs; ?>

                    </div>

                    <div class="text-muted">
                        Противопоказания:
                        <?php echo $info->contraindications; ?>

                    </div>

                    <div class="text-muted">
                        Звезды: <?php echo e($info->stars); ?>

                    </div>

                    <div class="text-muted">
                        Медицинские профили:
                        <?php echo e($info->payments_terms); ?>

                    </div>

                    <div class="text-muted">
                        Методы лечения:
                        <?php echo e($info->treatment_methods); ?>

                    </div>

                    <div class="text-muted">
                        Сервисы:
                        <?php echo e($info->services); ?>

                    </div>

                    <div class="text-muted">
                        Создан:
                        <?php echo e($info->created_at); ?>

                    </div>

                    <div class="text-muted">
                        Обновлен:
                        <?php echo e($info->updated_at); ?>

                    </div>
                </div>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->any(['info-update', 'info-delete'])): ?>
                    <div class="card-actions">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('info-update')): ?>
                            <a href="<?php echo e(route('info.edit', ['id' => $info->id])); ?>"><i class="fa fa-pen"></i></a>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('info-delete')): ?>
                            <form action="<?php echo e(route('info.destroy', ['id' => $info->id])); ?>" method="POST" class="d-inline ml-2">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('delete'); ?>
                                <button class="fa fa-times btn-unstyled"></button>
                            </form>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>