<?php $__env->startSection('title', 'Новости'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news-create')): ?>
            <a href="<?php echo e(route('news.create')); ?>">Создать</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row row-cards row-desk">
        <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3">
                <div class="card position-relative">
                    <a href="<?php echo e(route('news.edit', ['news' => $item->id])); ?>">
                        <img src="<?php echo e($item->image); ?>" alt="" class="card-img-top">
                    </a>
                    <div class="card-body d-flex flex-column">
                        <h4><a href="<?php echo e(route('news.show', ['id' => $item->id])); ?>"><?php echo e($item->title); ?></a></h4>
                        <div class="text-muted">
                            <?php echo e(str_limit($item->short_text, 100)); ?>

                        </div>
                    </div>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->any(['news-update'])): ?>
                        <div class="card-actions">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news-update')): ?>
                                <a href="<?php echo e(route('news.edit', ['id' => $item->id])); ?>"><i class="fa fa-pen"></i></a>
                            <?php endif; ?>
                            
                            
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php echo e($news->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>