<?php $__env->startSection('title', 'Объекты'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('object-create')): ?>
        <a href="<?php echo e(route('forms.create')); ?>">Создать</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Содержание</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $forms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="w-1"><?php echo e($form->id); ?></td>
                                    <td><?php echo e($form->form_name); ?></td>
                                    <td><?php echo e($form->content); ?></td>
                                    <td><?php echo e($form->created_at->format('m/d/Y')); ?></td>
                                    <td><?php echo e($form->updated_at->format('m/d/Y')); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($forms->links()); ?>

                    <form class="pagination-limit" action="/pagination_limit" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" name='pag_limit' class="form-control-sm" value="<?php echo e($limit); ?>" />
                        <input type="submit" class="btn btn-primary btn-sm" id="p-limit" value="Limit">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>