<?php $__env->startSection('page-content'); ?>
    <div class="container">
        <div class="row">
            <div class="col col-login mx-auto">
                <div class="text-center mb-6">
                    <img src="/" class="h-6" alt="">
                </div>

                <form class="card" action="<?php echo e(route('register')); ?>" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <div class="card-body p-6">
                        <div class="card-title">Создайте новый аккаунт</div>
                        <div class="form-group">
                            <label class="form-label">Имя</label>
                            <input type="text" class="form-control <?php echo e($errors->has('name') ? 'is-invalid' : ''); ?>" placeholder="Введите имя" name="name" >
                            <?php if($errors->has('name')): ?>
                                <small class="invalid-feedback">
                                    <?php echo e($errors->first('name')); ?>

                                </small>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <input type="email" class="form-control <?php echo e($errors->has('email') ? 'is-invalid' : ''); ?>" placeholder="Введите email" name="email" >
                            <?php if($errors->has('email')): ?>
                                <small class="invalid-feedback">
                                    <?php echo e($errors->first('email')); ?>

                                </small>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Пароль</label>
                            <input type="password" class="form-control <?php echo e($errors->has('password') ? 'is-invalid' : ''); ?>" placeholder="Пароль" name="password">
                            <?php if($errors->has('password')): ?>
                                <small class="invalid-feedback">
                                    <?php echo e($errors->first('password')); ?>

                                </small>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Подтвердите пароль</label>
                            <input type="password" class="form-control <?php echo e($errors->has('password_confirmation') ? 'is-invalid' : ''); ?>" placeholder="Подтверждение пароля" name="password_confirmation" >
                            <?php if($errors->has('password_confirmation')): ?>
                                <small class="invalid-feedback">
                                    <?php echo e($errors->first('password_confirmation')); ?>

                                </small>
                            <?php endif; ?>
                        </div>

                        
                        
                        
                        
                        
                        
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary btn-block">Создать новый аккаунт</button>
                        </div>
                    </div>
                </form>
                <div class="text-center text-muted small">
                    Уже есть аккаунт? <a href="<?php echo e(route('login')); ?>">Войдите!</a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('pages.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>