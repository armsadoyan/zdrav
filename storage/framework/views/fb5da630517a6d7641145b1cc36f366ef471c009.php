<?php $__env->startSection('title', 'Объекты'); ?>

<?php $__env->startSection('actions'); ?>
    <div class="links line-height-1">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('object-create')): ?>
            <a href="<?php echo e(route('objects.create')); ?>">Создать</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.parts.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row row-cards row-desk">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Список объектов</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Номера</th>
                                <th>Активен?</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $objects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="w-1"><?php echo e($object->id); ?></td>
                                    <td><a href="<?php echo e(route('objects.edit', ['id' => $object->id])); ?>"><?php echo e($object->name); ?></a></td>
                                    <td><?php echo e($object->created_at->format('m/d/Y')); ?></td>
                                    <td><?php echo e($object->updated_at->format('m/d/Y')); ?></td>
                                    <td><span class="tag tag-green">Номеров: <?php echo e($object->rooms->count()); ?></span></td>
                                    <td><span class="tag tag-<?php echo e($object->is_published ? 'green' : 'red'); ?>"><?php echo e($object->is_active ? 'Да' : 'Нет'); ?></span></td>
                                    <td class="form-inline">
                                        <a href="<?php echo e(route('objects.edit', ['id' => $object->id])); ?>" class="btn btn-sm btn-primary d-inline-block mr-1">Изменить</a>
                                        <form action="<?php echo e(route('objects.destroy', ['id' => $object->id])); ?>" method="POST" class="d-inline-block">
                                            <?php echo method_field('DELETE'); ?>
                                            <?php echo csrf_field(); ?>
                                            <button class="btn btn-sm btn-outline-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <?php echo e($objects->links()); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>