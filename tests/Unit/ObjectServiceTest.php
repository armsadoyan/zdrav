<?php

namespace Tests\Unit;

use App\Models\Place;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ObjectServiceTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAll()
    {
        $place = factory(Place::class)->create();
        $this->assertDatabaseHas('places', $place->toArray());
    }
}
